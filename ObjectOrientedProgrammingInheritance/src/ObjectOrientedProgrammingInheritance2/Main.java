package ObjectOrientedProgrammingInheritance2;

public class Main {
    public static void main(String[] args){
        WritingMaterials wm = new WritingMaterials("ручка", "Красный", 145, 15.6, true);
        wm.display();

        Pen p = new Pen();
        p.setName("Паркер");
        p.display();
        p.setCountColor(2);
        p.setAuto(false);
        System.out.println(p.getCountColor());
        System.out.println(p.isAuto());
        p.writeMyName();

        Ruler r = new Ruler();
        r.setName("Линейка");
        r.display();
        r.setLength(25);
        r.setWood(true);
        System.out.println(r.getLength());
        System.out.println(r.isWood());
        r.measure();

        Divider d = new Divider();
        d.setName("Циркуль");
        d.display();
        d.setDividerType("С карандашом");
        d.setMetal(true);
        System.out.println(d.getDividerType());
        System.out.println(d.isMetal());
        d.drawCircle();

        WritingMaterials.getDescription();
        System.out.println(wm);
        System.out.println(p);
        System.out.println(r.toString());
        System.out.println(d);
        System.out.printf("Количество канц товаров - %d\n", WritingMaterials.getCounter());

    }
}
