package ObjectOrientedProgrammingInheritance2;

public final class Ruler extends WritingMaterials {
    private int length;
    private boolean isWood;

    {
        length = 0;
        isWood = false;
    }

    Ruler() {
        this.isWood = false;
        this.setDraw(false);
    }

    @Override
    public double getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean isWood() {
        return isWood;
    }

    public void setWood(boolean wood) {
        isWood = wood;
    }

    public void measure() {
        System.out.println("Сейчас померяем длину");
    }

    @Override
    public void display() {
        String answerByDraw = (this.isDraw()) ? "Да" : "Нет";
        String answerByMaterial = (this.isWood()) ? "Да" : "Нет";
        System.out.printf("This is %s, Название предмета %s, Цвет: %s, Длина: %s, Цена: %d, Умеет рисовать: %s, Длинна Линейки: %s, Состоит из дерева: %s \n",
                this.getClass().getSimpleName(),
                this.getName(),
                this.getColor(),
                this.getLength(),
                this.getPrice(),
                answerByDraw,
                this.getLength(),
                answerByMaterial);
    }

}
