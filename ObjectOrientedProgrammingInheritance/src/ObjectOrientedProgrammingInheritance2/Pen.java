package ObjectOrientedProgrammingInheritance2;

public class Pen extends WritingMaterials {
    private int countColor;
    private boolean isAuto;

    {
        countColor = 0;
        isAuto = false;
    }

    Pen() {
        super.setDraw(true);
    }

    public int getCountColor() {
        return this.countColor;
    }

    public void setCountColor(int countColor) {
        this.countColor = countColor;
    }

    public boolean isAuto() {
        return isAuto;
    }

    public void setAuto(boolean auto) {
        isAuto = auto;
    }

    public void writeMyName() {
        System.out.println("MadnessLalka");
    }

    @Override
    public void display() {
        String answerByDraw = (this.isDraw()) ? "Да" : "Нет";
        String answerByAuto = (this.isAuto()) ? "Да" : "Нет";
        System.out.printf("This is %s, Название предмета %s, Цвет: %s, Длина: %s, Цена: %d, Умеет рисовать: %s, Количество цветов: %d, Автоматическая?: %s \n",
                this.getClass().getSimpleName(),
                this.getName(),
                this.getColor(),
                this.getLength(),
                this.getPrice(),
                answerByDraw,
                countColor,
                answerByAuto);
    }
}
