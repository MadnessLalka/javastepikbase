package ObjectOrientedProgrammingInheritance2;

public class Divider extends WritingMaterials {
    private String dividerType;
    private boolean isMetal;

    {
        dividerType = "None";
        isMetal = false;
    }

    Divider() {
        super.setDraw(true);
    }

    public String getDividerType() {
        return dividerType;
    }

    public void setDividerType(String dividerType) {
        this.dividerType = dividerType;
    }

    public boolean isMetal() {
        return isMetal;
    }

    public void setMetal(boolean metal) {
        isMetal = metal;
    }

    public final void drawCircle() {
        System.out.println("Нарисован круг");
    }

    @Override
    public void display(){
        String answerByDraw = (this.isDraw()) ? "Да" : "Нет";
        String answerByMaterial = (this.isMetal()) ? "Да" : "Нет";
        System.out.printf("This is %s, Название предмета: %s, Цвет: %s, Длина: %s, Цена: %d, Умеет рисовать: %s, Тип Циркуля: %s, Состоит из метала: %s \n",
                this.getClass().getSimpleName(),
                this.getName(),
                this.getColor(),
                this.getLength(),
                this.getPrice(),
                answerByDraw,
                this.getDividerType(),
                answerByMaterial);
    }

}
