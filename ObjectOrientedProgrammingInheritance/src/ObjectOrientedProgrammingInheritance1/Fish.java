package ObjectOrientedProgrammingInheritance1;

public final class Fish extends Animal {
    private String squame;
    private boolean isUpStreamSwim;
    private int id;
    private static int counter = 1;

    @Override
    public int getId() {
        return id;
    }

    public static int getCounter() {
        return counter;
    }

    {
        squame = "Неизвество";
        isUpStreamSwim = false;
        id = counter++;
    }

    Fish(){
        super.setType("Рыба");
        super.setSwim(true);
    }

    public void setSwim() {
        super.setSwim(true);
    }

    public String getSquame() {
        return squame;
    }

    public void setSquame(String squame) {
        this.squame = squame;
    }

    public boolean isUpStreamSwim() {
        return isUpStreamSwim;
    }

    public void setUpStreamSwim(boolean upStreamSwim) {
        isUpStreamSwim = upStreamSwim;
    }

    public void bulBul(){
        System.out.println("Bul-bul");
    }

    @Override
    public void display(){
        System.out.println("I am a fish");
        super.display();
    }


}
