package ObjectOrientedProgrammingInheritance1;

public class Insect extends Animal {
    private int wingCount;
    private boolean islikeJesus;
    private int id;
    private static int counter = 1;

    {
        wingCount = 0;
        islikeJesus = false;
        id = counter++;
    }

    Insect() {
        super.setType("Насекомое");
        super.setWalk(true);
    }

    public int getWingCount() {
        return wingCount;
    }

    public void setWingCount(int wingCount) {
        this.wingCount = wingCount;
    }

    public boolean isLikeJesus() {
        return islikeJesus;
    }

    public void setLikeJesus(boolean islikeJesus) {
        this.islikeJesus = islikeJesus;
    }

    public void gGGGG() {
        System.out.println("Ggggg");
    }

    @Override
    public void display(){
        System.out.println("I am insect");
    }

    public boolean isIslikeJesus() {
        return islikeJesus;
    }

    @Override
    public int getId() {
        return id;
    }

    public static int getCounter() {
        return counter;
    }
}
