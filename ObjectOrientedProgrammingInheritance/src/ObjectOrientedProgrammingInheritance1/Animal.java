package ObjectOrientedProgrammingInheritance1;

public class Animal {
    public int getId() {
        return id;
    }

    public static int getCounter() {
        return counter;
    }

    private String type;
    private String name;
    private int age;
    private double weight;
    private boolean isFly;
    private boolean isWalk;
    private boolean isSwim;
    private int id;
    private static int counter = 1;

    static final private String description = "Описание класса Animal";

    {
        name = "Бобик";
        type = "Животное";
        age = 0;
        weight = 0;


    }

    Animal(){
        id = counter++;
    }

    Animal(String type, String name) {
        this.type = type;
        this.name = name;
    }

    Animal(String type, int age) {

        this.type = type;
        this.age = age;
        this.name = "No Name";
    }

    Animal(String type, String name, int age, double weight, boolean isFly, boolean isWalk, boolean isSwim) {
        this.type = type;
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.isFly = isFly;
        this.isWalk = isWalk;
        this.isSwim = isSwim;
    }

    public static void getDescription(){
        System.out.println(description);
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public int getAge() {
        return this.age;
    }

    public double getWeight() {
        return this.weight;
    }

    public boolean isFly() {
        return this.isFly;
    }

    public boolean isWalk() {
        return this.isWalk;
    }

    public boolean isSwim() {
        return this.isSwim;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setFly(boolean isFly) {
        this.isFly = isFly;
    }

    public void setWalk(boolean isWalk) {
        this.isWalk = isWalk;
    }

    public void setSwim(boolean isSwim) {
        this.isSwim = isSwim;
    }


    public void display() {
        String answerByFly = (isFly) ? "Да" : "Нет";
        String answerByWalk = (isWalk) ? "Да" : "Нет";
        String answerBySwim = (isSwim) ? "Да" : "Нет";

        System.out.println("Тип: " + type + ", Имя:  " + name + ", Возраст: " + age + ", Вес: " + weight + ", Умеет летать: " + answerByFly + ", Умеет ходить: " + answerByWalk + ", Умеет плавать: " + answerBySwim);
    }


   final public void rename(String name) {
        this.name = name;
    }

    public void holiday(int holidayCount) {
        for (int i = 0; i < holidayCount; i++) {
            this.weight += 0.1;
        }
    }

    public void holiday() {
        this.weight += 0.1;
    }

    public void holiday(double m) {
        this.weight += m;
    }

    public void holiday(double m, int n) {
        for (int i = 0; i < n; i++) {
            this.weight += m;
        }
    }

    @Override
    public String toString() {
        return "Animal{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                ", isFly=" + isFly +
                ", isWalk=" + isWalk +
                ", isSwim=" + isSwim +
                ", id=" + id +
                '}';
    }
}
