package ObjectOrientedProgrammingInheritance1;

public class Main {
    public static void main(String[] args) {
        Bird vorobey = new Bird("Птица", "Джек", 28, 0.5, true, false, false);
        System.out.println("Общее количество животных - " + Animal.getCounter());
        System.out.println("Номер животного - " + vorobey.getId());
        System.out.println(vorobey.toString());
        Insect zhora = new Insect();
        System.out.println("Общее количество животных - " + Animal.getCounter());
        System.out.println("Номер животного - " + zhora.getId());
        System.out.println(zhora);

    }
}