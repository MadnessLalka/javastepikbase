package ObjectOrientedProgrammingInheritance1;

public class Bird extends Animal {
    private String area;
    private boolean isWinterFly;
    private int id;
    private static int counter = 1;



    public Bird(String type, String name, int age, double weight, boolean isFly, boolean isWalk, boolean isSwim) {
        super(type, name, age, weight, isFly, isWalk, isSwim);
    }

    public Bird(String type, int age) {
        super(type, age);
    }

    public Bird(String type, String name) {
        super(type, name);
    }

    @Override
    public int getId() {
        return id;
    }

    public static int getCounter() {
        return counter;
    }

    {
        area = "РФ";
        isWinterFly = true;
        id = counter++;
    }

    public Bird() {
        super.setType("Птица");
        super.setFly(true);
    }

    public void chirikChirik() {
        System.out.println("Chirik-Chirik");
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public boolean isWinterFly() {
        return isWinterFly;
    }

    public void setWinterFly(boolean winterFly) {
        isWinterFly = winterFly;
    }

    @Override
    public void display(){
        String answerByWinterFly = (isWinterFly) ? "Да" : "Нет";

        System.out.println("I am ObjectOrientedProgrammingInheritance1.Bird");
        super.display();
    }

}
