import java.util.Scanner;

public class ConditionalStatements1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int time = sc.nextInt();
        int speed = sc.nextInt();

        System.out.println(time * speed);

    }
}