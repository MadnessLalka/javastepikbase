import java.util.Scanner;

class ConditionalStatements12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double airTemperature = sc.nextDouble();

        if (airTemperature < 22.4) {
            System.out.println("Холодно(");
        } else if (airTemperature == 22.4) {
            System.out.println("Прохладно...");
        } else if (airTemperature > 22.4) {
            System.out.println("Тепло!");
        }
    }
}