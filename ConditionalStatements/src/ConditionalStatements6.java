import java.util.Scanner;

public class ConditionalStatements6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int hoursWhichRiseUp = sc.nextInt();
        int minutesWhichRiseUp = sc.nextInt();
        int secondTime = sc.nextInt();

        int secondWhichRiseUp = (hoursWhichRiseUp * 60 * 60) + (minutesWhichRiseUp * 60);

        boolean isPetyaGetUp = secondTime <= secondWhichRiseUp;

        if (isPetyaGetUp) {
            System.out.println("Успел");
        } else {
            System.out.println("Опоздал");
        }
    }
}