import java.util.Scanner;

public class ConditionalStatements3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String str1 = sc.nextLine();
        String str2 = sc.nextLine();
        String str3 = sc.nextLine();

        if (str1.equals(str2) && str1.equals(str3)){
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }


    }
}