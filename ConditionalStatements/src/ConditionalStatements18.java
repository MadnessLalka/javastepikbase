import java.util.Scanner;

class ConditionalStatements18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        float x = sc.nextFloat();
        float y = sc.nextFloat();

        if (y > x * 2){
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }
}