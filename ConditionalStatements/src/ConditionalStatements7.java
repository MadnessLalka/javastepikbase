import java.util.Scanner;

public class ConditionalStatements7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();

        boolean isMaxNumberA = (a >= b) && (a >= c);
        boolean isMaxNumberB = (b >= a) && (b >= c);
        boolean isMaxNumberC = (c >= b) && (c >= a);

        if (isMaxNumberA) {
            System.out.println(a);
        } else if (isMaxNumberB) {
            System.out.println(b);
        } else if (isMaxNumberC) {
            System.out.println(c);
        }
    }
}