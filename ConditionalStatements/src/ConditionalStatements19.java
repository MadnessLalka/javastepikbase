import java.util.Scanner;

class ConditionalStatements19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        float x = sc.nextFloat();
        float y = sc.nextFloat();

        if (y < (3 * x) && y > (2 * x)) {
            System.out.print("Yes");
        } else {
            System.out.print("No");
        }
    }
}