import java.util.Scanner;

class ConditionalStatements13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int intNumber = sc.nextInt();

        if (intNumber > 0 && intNumber < 1000) {
            if (intNumber < 100 && intNumber / 10 != 0) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        } else {
            System.out.println("No");
        }
    }
}