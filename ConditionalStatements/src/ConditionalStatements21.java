import java.util.Scanner;

class ConditionalStatements21 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        float x = sc.nextFloat();
        float y = sc.nextFloat();

        if ((5 > ((y * y) + (x * x)) && y < ((x * x) - 3) && (y > (0.5 * x - 1))) || (5 < ((y * y) + (x * x)) && y > ((x * x) - 3) && (y < (0.5 * x - 1)))) {
            System.out.print("Yes");
        } else {
            System.out.print("No");
        }
    }
}