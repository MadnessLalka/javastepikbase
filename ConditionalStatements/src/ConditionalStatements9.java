import java.util.Scanner;

public class ConditionalStatements9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String a = sc.nextLine();
        String b = sc.nextLine();
        String c = sc.nextLine();

        boolean isEqualA = (a.equals(b) && !a.equals(c)) || (!a.equals(b) && a.equals(c));
        boolean isEqualB = (b.equals(a) && !b.equals(c)) || (!b.equals(a) && b.equals(c));
        boolean isEqualC = (c.equals(a) && !c.equals(b)) || (!c.equals(a) && c.equals(b));

        if (isEqualA || isEqualB || isEqualC) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }
}