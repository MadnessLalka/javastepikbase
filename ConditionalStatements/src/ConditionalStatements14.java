import java.util.Scanner;

class ConditionalStatements14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int intNumber = sc.nextInt();

        if (intNumber > 0 && intNumber < 1000) {
            if (intNumber < 10) {
                System.out.println(1);
            } else if (intNumber <= 99) {
                System.out.println(2);
            } else {
                System.out.println(3);
            }
        }
    }
}