import java.util.Scanner;

public class ConditionalStatements4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double num1 = sc.nextFloat();
        double num2 = sc.nextFloat();

        boolean isModNumbers = num2 > 0;

        if (isModNumbers) {
            double quotientOfANumber = num1 / num2;
            System.out.println(quotientOfANumber);
        } else {
            System.out.println("Error");
        }
    }
}