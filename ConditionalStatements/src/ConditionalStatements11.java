import java.util.Scanner;

class ConditionalStatements11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int N = sc.nextInt();
        int M = sc.nextInt();
        int x = sc.nextInt();
        int y = sc.nextInt();

        if (x > y) {
            System.out.println(y);
        } else {
            System.out.println(x);
        }
    }
}