import java.util.Scanner;

class ConditionalStatements16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();

        if (a != 0) {
            int D = (b * b) - (4 * a * c);

            if (D < 0) {
                System.out.println(0);
            } else if (D == 0) {
                System.out.println(1);
            } else {
                System.out.println(2);
            }
        }
    }
}