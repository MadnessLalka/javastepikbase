import java.util.Scanner;

class ConditionalStatements20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        float x = sc.nextFloat();
        float y = sc.nextFloat();

        if (5 > ((y * y) + (x * x)) && y < ((x * x) - 3)) {
            System.out.print("Yes");
        } else {
            System.out.print("No");
        }
    }
}