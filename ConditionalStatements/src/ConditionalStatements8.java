import java.util.Scanner;

public class ConditionalStatements8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int year = sc.nextInt();
        boolean isMod400 = year % 400 == 0; //true
        boolean isMod4 = year % 4 == 0; //true
        boolean isMod100 = year % 100 == 0;

        if (isMod4) {
            if (isMod100) {
                if (isMod400) {
                    System.out.println("Yes");
                } else {
                    System.out.println("No");
                }
            } else {
                System.out.println("Yes");
            }
        } else {
            System.out.println("No");
        }
    }
}