import java.util.Scanner;

public class ConditionalStatements5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int countMotherPies = sc.nextInt();

        int countPiesAfterHunters = countMotherPies - 10;
        int countPiesAfterWolf = countMotherPies / 2;

        if (countPiesAfterHunters > countPiesAfterWolf) {
            System.out.println(countPiesAfterHunters);
        } else {
            System.out.println(countPiesAfterWolf);
        }
    }
}