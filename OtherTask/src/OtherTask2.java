import java.util.Scanner;

public class OtherTask2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt(), b = scanner.nextInt(), c = scanner.nextInt();

        if (a != 0 ){
            int D = (b * b) - 4 * a * c;
            if(D > 0){
                System.out.println(2);
            } else if (D == 0) {
                System.out.println(1);
            } else if(D < 0){
                System.out.println(0);
            }
        }

    }
}