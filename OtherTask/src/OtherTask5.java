import java.util.Scanner;

public class OtherTask5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        Float[] arrayEvenNumbers = new Float[n];

        for (int i = 0; i < n; i++) {
            arrayEvenNumbers[i] = scanner.nextFloat();
        }

        Float maxNumber = arrayEvenNumbers[0], minNumber = arrayEvenNumbers[0];
        int indexMaxNumber = 0, indexMinNumber = 0;

        for(int i = 0; i < arrayEvenNumbers.length; i++){
            if(arrayEvenNumbers[i] > maxNumber) { maxNumber = arrayEvenNumbers[i]; indexMaxNumber = i; }
            if(arrayEvenNumbers[i] < minNumber) { minNumber = arrayEvenNumbers[i]; indexMinNumber = i; }
        }

        arrayEvenNumbers[indexMaxNumber] = minNumber;
        arrayEvenNumbers[indexMinNumber] = maxNumber;

        for (Float num: arrayEvenNumbers){
            System.out.print(num + " ");
        }
    }
}