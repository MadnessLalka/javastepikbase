import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class OtherTask7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<String> newLine = new ArrayList<>();

        newLine.addAll(List.of(scanner.nextLine().toLowerCase().split(" ")));
        int counterSymbolA = 0;

        for (String word : newLine) {
            char[] tempArray = word.toCharArray();
            if (tempArray[0] == 'а') {
                counterSymbolA++;
            }
        }
        System.out.println(counterSymbolA);

    }
}