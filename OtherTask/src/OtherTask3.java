import java.util.Scanner;
import java.util.SortedMap;

public class OtherTask3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int maxNumber = 0;

        while (true) {
            int number = scanner.nextInt();
            if (number != 0) {
                if (number % 3 == 0 && number > maxNumber) {
                    maxNumber = number;
                }
            } else {
                break;
            }
        }

        if (maxNumber == 0) {
            System.out.println("404");
        } else {
            System.out.println(maxNumber);
        }
    }
}