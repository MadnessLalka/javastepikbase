import java.util.Scanner;

public class OtherTask6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt(), m = scanner.nextInt();
        int[][] matrix = new int[n][m];
        int column = 1;
        int line = 1;
        boolean isLineEqual = false;
        boolean isColumEqual = false;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = scanner.nextInt();
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                line *= matrix[i][j];

            }
            if (line == 15) {
                isLineEqual = true;
            }
            line = 1;
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                column *= matrix[j][i];

            }
            if (column == 15) {
                isColumEqual = true;
            }
            column = 1;
        }

        System.out.println(isLineEqual ? "YES" : "NO");
        System.out.println(isColumEqual ? "YES" : "NO");


    }
}