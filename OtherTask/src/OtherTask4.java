import java.util.Scanner;

public class OtherTask4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt(), b = scanner.nextInt();
        int sumNumber = 0;

        for (int i = a; i <= b ; i++) {
            if (i % 10 == 3 || i % 10 == 7) sumNumber+= i;
        }
        System.out.println(sumNumber);

    }
}