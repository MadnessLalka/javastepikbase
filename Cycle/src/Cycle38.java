import java.util.Scanner;

public class Cycle38 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int number;
        long multiNumbers = 1;
        for (int i = 0; i < n; i++) {
            number = sc.nextInt();
            multiNumbers *= number;
        }
        System.out.println(multiNumbers);
    }
}