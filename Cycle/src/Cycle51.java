import java.util.Scanner;

public class Cycle51 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int counterOtherNumbers = 0;
        int number;

        while ((number = scan.nextInt()) != 0) {
            if (number % 3 == 0) {
                counterOtherNumbers++;
            }
            if (number < 0) {
                counterOtherNumbers--;
            }
        }

        if (counterOtherNumbers > 0) {
            System.out.println("333");
        } else if (counterOtherNumbers == 0) {
            System.out.println("Equal");
        } else {
            System.out.println("negative");
        }
    }
}