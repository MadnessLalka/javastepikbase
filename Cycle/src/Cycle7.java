import java.util.Scanner;

public class Cycle7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int divCounter = 2;
        if (n >= 2) {
            while (n % divCounter != 0) {
                divCounter++;
            }
            System.out.println(divCounter);
        }

    }
}