import java.util.Scanner;

public class Cycle46 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int intNumber = scan.nextInt();
        int multiDigitsOfNumber = 1;
        if (intNumber > 0) {
            while (intNumber != 0) {
                multiDigitsOfNumber *= intNumber % 10;
                intNumber /= 10;
            }
            System.out.println(multiDigitsOfNumber);
        }
    }
}