import java.util.Scanner;

public class Cycle21 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int numberOfStars = sc.nextInt();
        int stringCounter = 0;
        while (stringCounter < numberOfStars) {
            int i = 0;
            while (i <= stringCounter) {
                System.out.print("*");
                i++;
            }
            System.out.println();
            stringCounter++;
        }

    }
}