import java.util.Scanner;

public class Cycle24 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int countOfNumbers = sc.nextInt();
        int counter = 0;
        int counterOfEvenNumbers = 0;

        while (counter < countOfNumbers) {
            if (sc.nextInt() % 2 == 0) {
                counterOfEvenNumbers++;
            }
            counter++;
        }
        System.out.println(counterOfEvenNumbers);

    }
}