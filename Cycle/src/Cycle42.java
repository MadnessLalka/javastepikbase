import java.util.Scanner;

public class Cycle42 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int a = scan.nextInt(), b = scan.nextInt(), c = scan.nextInt();

        for (int i = a; i >= b; i -= c) {
            System.out.println(i);
        }
    }
}