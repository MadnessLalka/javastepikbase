import java.util.Scanner;

public class Cycle25 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int number;
        int numberSum = 0;
        for (; ; ) {
            number = sc.nextInt();
            if (number % 7 != 0) {
                if (number % 10 == 7) {
                    numberSum += number;
                }
            } else {
                break;
            }
        }
        System.out.println(numberSum);
    }
}