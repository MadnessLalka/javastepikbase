import java.util.Scanner;

public class Cycle10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int numberSum = 0;

        while (n != 0){
            numberSum += n % 10;
            n /= 10;
        }
        System.out.println(numberSum);
    }
}