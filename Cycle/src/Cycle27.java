import java.util.Scanner;

public class Cycle27 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int amountNumbers = sc.nextInt();
        int intNumber = sc.nextInt();
        int minNumber = intNumber;
        int maxNumber = intNumber;

        for (int i = 0; i < amountNumbers - 1; i++) {
            intNumber = sc.nextInt();
            if (intNumber > maxNumber) {
                maxNumber = intNumber;
            }
            if (intNumber < minNumber) {
                minNumber = intNumber;
            }
        }
        System.out.print(minNumber + "\n" + maxNumber);
    }
}