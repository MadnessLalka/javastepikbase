import java.util.Scanner;

public class Cycle18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int sumN = n;

        while (n != 0) {
            n = sc.nextInt();
            sumN += n;
        }
        System.out.println(sumN);

    }
}