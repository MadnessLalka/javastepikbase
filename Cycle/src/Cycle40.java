import java.util.Scanner;

public class Cycle40 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int a = scan.nextInt(), b = scan.nextInt();
        long multiAtoB = 1;

        if (a <= b)
            for (int i = a; i <= b; i++) {
                multiAtoB *= i;
            }
        System.out.println(multiAtoB);
    }
}