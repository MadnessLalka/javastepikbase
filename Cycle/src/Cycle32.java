import java.util.Scanner;

public class Cycle32 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String word = "";
        int counterWord = 0;
        boolean hasCubeWord = false;

        while (!word.equals("СТОП")) {
            word = sc.next();
            if (!(word.equals("ээээ") || word.equals("потом"))) {
                counterWord++;
                if (word.equals("Куб")) {
                    hasCubeWord = true;
                    break;
                }
            }
        }

        if (hasCubeWord) {
            System.out.println(counterWord);
        } else {
            System.out.println("NO");
        }

    }
}