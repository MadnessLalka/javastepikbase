import java.util.Scanner;

public class Cycle16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int k = sc.nextInt(), n = sc.nextInt();

        if ((k >= 1 && k <= 10) && (n >= 0 && n <= 100)) {
            int nCounter = 1;
            while (nCounter <= n) {
                if (nCounter % k == 0){
                    System.out.println(nCounter);
                }
                nCounter++;
            }
        }

    }
}