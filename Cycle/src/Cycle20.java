import java.util.Scanner;

public class Cycle20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int counterNumbers = 0;
        int productOfNumbers = 1;

        if (n == 1) {
            productOfNumbers *= sc.nextInt();
            counterNumbers++;
        } else {
            while (productOfNumbers < n) {
                productOfNumbers *= sc.nextInt();
                counterNumbers++;
            }
        }
        System.out.println(counterNumbers + " " + productOfNumbers);

    }
}