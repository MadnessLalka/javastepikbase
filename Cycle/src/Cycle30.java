import java.util.Scanner;

public class Cycle30 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int a = sc.nextInt(), n = sc.nextInt();
        int geometricProgressive = 0;
        if (n > 0) {
            for (int i = 0; i <= n; i++) {
                geometricProgressive += (int) Math.pow(a, i);
            }
            System.out.println(geometricProgressive);
        }

    }
}