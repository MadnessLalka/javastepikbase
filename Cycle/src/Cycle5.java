import java.util.Scanner;

public class Cycle5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int maxNumber = n;

        while (n != 0) {
            n = sc.nextInt();
            if (n > maxNumber) {
                maxNumber = n;
            }
        }
        System.out.println(maxNumber);

    }
}