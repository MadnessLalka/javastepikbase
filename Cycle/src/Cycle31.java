import java.util.Scanner;

public class Cycle31 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int number = sc.nextInt();
        String binaryNumber = "";

        while (true) {
            binaryNumber += number % 2;
            if (number / 2 == 0) {
                break;
            }
            number /= 2;
        }
        System.out.println(binaryNumber);
    }
}