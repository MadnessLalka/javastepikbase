import java.util.Scanner;

public class Cycle4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int numberCounter = 0;
        double sumNumber = n;

        while (n != 0) {
            n = sc.nextInt();
            sumNumber += n;
            numberCounter++;
        }

        System.out.println((sumNumber / numberCounter));

    }
}