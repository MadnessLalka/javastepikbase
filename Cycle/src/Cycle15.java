import java.util.Scanner;

public class Cycle15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int a = sc.nextInt(), b = sc.nextInt();

        if (a > b) {
            while (a >= b) {
                System.out.println(a--);
            }
        } else {
            while (a <= b) {
                System.out.println(a++);
            }
        }



    }
}