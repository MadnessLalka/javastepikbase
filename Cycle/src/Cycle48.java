import java.util.Scanner;

public class Cycle48 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int countNumbers = scan.nextInt();
        boolean isNumberEndsFour = false;
        int number;

        for (int i = 0; i < countNumbers; i++) {
            number = scan.nextInt();
            if (number % 10 == 4) {
                isNumberEndsFour = true;
            }
        }

        if (isNumberEndsFour) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }

    }
}