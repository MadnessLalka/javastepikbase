import java.util.Scanner;

public class Cycle45 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int countOfArt = scan.nextInt();
        String asciiOwl =
                       ":)\\_____/(:\n" +
                        " {(@)v(@)}\n" +
                        " {|~- -~|}\n" +
                        " {/^'^'^\\}\n" +
                        " ===m-m===\n";

        for (int i = 0; i < countOfArt; i++) {
            System.out.println(asciiOwl);
        }
    }
}