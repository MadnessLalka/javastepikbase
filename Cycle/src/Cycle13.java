import java.util.Scanner;

public class Cycle13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int number = sc.nextInt();
        int numberCounter = 1;
        if (number > 0) {
            while (numberCounter <= number) {
                System.out.println(numberCounter++);
            }
        }

    }
}