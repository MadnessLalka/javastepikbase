import java.util.Scanner;

public class Cycle22 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int numbersOfLayers = sc.nextInt();
        int counterOfLayers = 0;

        while (counterOfLayers < numbersOfLayers) {
            int counterOfStars = 1;
            int numberOfStarts = numbersOfLayers + counterOfLayers;
            while (counterOfStars <= numberOfStarts) {
                if (counterOfStars >= (numbersOfLayers - counterOfLayers)) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
                counterOfStars++;
            }
            System.out.println();
            counterOfLayers++;
        }

    }
}