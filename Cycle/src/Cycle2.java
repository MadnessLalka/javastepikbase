import java.util.Scanner;

public class Cycle2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int a = sc.nextInt(), b = sc.nextInt();

        if (a <= b) {
            int counter = a;
            int sum = 0;

            while (counter <= b) {
                sum += counter;
                counter++;
            }
            System.out.println(sum);
        }

    }
}