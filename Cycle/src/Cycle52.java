import java.util.Scanner;

public class Cycle52 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int countOfKids = scan.nextInt();
        int kidsHeight, maxKidsHeight = 0, minKidsHeight = 180;
        String kidsName, maxKidsName = "", minKidsName = "";

        while (countOfKids != 0) {
            kidsName = scan.next();
            kidsHeight = scan.nextInt();
            if (kidsHeight > maxKidsHeight) {
                maxKidsHeight = kidsHeight;
                maxKidsName = kidsName;
            }
            if (kidsHeight < minKidsHeight) {
                minKidsHeight = kidsHeight;
                minKidsName = kidsName;
            }
            countOfKids--;
        }
        System.out.println(minKidsName + "\n" + maxKidsName);

    }
}