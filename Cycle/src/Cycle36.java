import java.util.Scanner;

public class Cycle36 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int sumEvenNumbers = 0;
        for (int i = 1; i <= n; i++) {
            sumEvenNumbers += (int)Math.pow(i, 2);
        }
        System.out.println(sumEvenNumbers);
    }
}