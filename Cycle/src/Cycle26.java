import java.util.Scanner;

public class Cycle26 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean isWordCube = false;
        String word;
        while (true) {
            word = sc.next();
            if (!(word.equals("СТОП"))) {
                if (word.equals("Куб")) {
                    isWordCube = true;
                }
            } else {
                break;
            }
        }

        if (isWordCube) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }

    }
}