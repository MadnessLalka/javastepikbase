import java.util.Scanner;

public class Cycle9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double x = sc.nextDouble();
        int y = sc.nextInt();
        int dayCounter = 1;

        while (x < y) {
            x += x * 0.1;
            dayCounter++;
        }
        System.out.println(dayCounter);

    }
}