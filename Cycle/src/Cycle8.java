import java.util.Scanner;

public class Cycle8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int counterN = 0;
        int sumN = 0;

        while (counterN<=n){
            sumN += (int)Math.pow(counterN, 2);
            counterN++;
        }
        System.out.println(sumN);

    }
}