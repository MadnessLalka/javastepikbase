import java.util.Scanner;

public class Cycle17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int counterN = 0;

        while (n != 0){
            n = sc.nextInt();
            counterN++;
        }
        System.out.println(counterN);

    }
}