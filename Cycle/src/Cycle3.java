import java.util.Scanner;

public class Cycle3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int a = sc.nextInt(), b = sc.nextInt();
        int countMultip = 0;
        if (a <= b) {
            while (a <= b) {
                if (a % 3 == 0 && a % 5 != 0) {
                    countMultip++;
                }
                a++;
            }
        }
        System.out.println(countMultip);

    }
}