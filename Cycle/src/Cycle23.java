import java.util.Scanner;

public class Cycle23 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int countOfNumbers = sc.nextInt();
        int counter = 0;
        int sumOfNumbers = 0;

        while (counter < countOfNumbers){
            sumOfNumbers += sc.nextInt();
            counter++;
        }
        System.out.println(sumOfNumbers);

    }
}