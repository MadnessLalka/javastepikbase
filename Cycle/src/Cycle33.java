import java.util.Scanner;

public class Cycle33 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int number;
        int countEqualMaximumNumber = 0;
        int maximumNumber = 0;

        for (int i = 0; i < 10000; i++) {
            number = sc.nextInt();
            if (number <= 0) {
                System.out.println(countEqualMaximumNumber);
                break;
            } else {
                if (number > maximumNumber) {
                    maximumNumber = number;
                    countEqualMaximumNumber = 1;
                } else if (number == maximumNumber) {
                    countEqualMaximumNumber++;
                }
            }
        }
    }
}