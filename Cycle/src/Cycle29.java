import java.util.Scanner;

public class Cycle29 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String data;
        int numberMultiplication = 1;
        boolean hasAllNumbersZero = false;

        for (; ; ) {
            data = sc.next();
            if (!(data.equals("СТОП"))) {

                int IntegerNumber = Integer.parseInt(data);
                if (IntegerNumber != 0) {
                    numberMultiplication *= IntegerNumber;
                    hasAllNumbersZero = true;
                }
            } else {
                break;
            }
        }

        if (hasAllNumbersZero) {
            System.out.println(numberMultiplication);
        } else {
            System.out.println("Не найдено");
        }
    }
}