import java.util.Scanner;

public class Cycle49 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int counter = 1;
        while (true) {
            if (counter <= 5) {
                String correct = scan.nextLine();
                String vasya = scan.nextLine();
                if (correct.equals(vasya)) {
                    System.out.println("CORRECT");
                    break;
                } else {
                    System.out.println("INCORRECT" + counter);
                }
            } else {
                System.out.println("Error");
                break;
            }
            counter++;
        }
    }
}