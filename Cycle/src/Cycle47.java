import java.util.Scanner;

public class Cycle47 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int a = scan.nextInt(), b = scan.nextInt();

        if (a <= b) {
            for (int i = b; i >= a; i -= 2) {
                System.out.println(i);
            }
        }


    }
}