import java.util.Scanner;

public class Cycle11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int number = sc.nextInt();
        int lastNumber = number;
        int counterMoreNumbers = 0;

        while (number != 0) {
            number = sc.nextInt();
            if (lastNumber < number) {
                counterMoreNumbers++;
            }
            lastNumber = number;
        }
        System.out.println(counterMoreNumbers);

    }
}