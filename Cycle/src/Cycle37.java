import java.util.Scanner;

public class Cycle37 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt(), m = sc.nextInt();
        if (n <= m){
            for (int i = n; i <= m; i++) {
                System.out.println(i + " " + i + ".$");
            }
        }

    }
}