import java.util.Scanner;

public class Cycle43 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int countOfSchoolKids = scan.nextInt();
        double kidsSumYears = 0;

        for (int i = 0; i < countOfSchoolKids; i++) {
            String kidName = scan.next();
            int kidYear = scan.nextInt();
            kidsSumYears += kidYear;
        }
        System.out.println(kidsSumYears / countOfSchoolKids);

    }
}