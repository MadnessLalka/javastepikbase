import java.util.Scanner;

public class Cycle12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int number = sc.nextInt();
        int smallerAfterMaxNumber = 0;
        int maxNumber = number;

        while (number != 0) {
            number = sc.nextInt();
            if (maxNumber < number) {
                smallerAfterMaxNumber = maxNumber;
                maxNumber = number;
            } else if (maxNumber > number && smallerAfterMaxNumber < number) {
                smallerAfterMaxNumber = number;
            }
        }
        System.out.println(smallerAfterMaxNumber);

    }
}