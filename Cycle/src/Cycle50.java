import java.util.Scanner;

public class Cycle50 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int alexYear = scan.nextInt(), brotherAlexYear = scan.nextInt();

        if (brotherAlexYear > alexYear) {
            int counterYears = 0;
            while (true) {
                if (brotherAlexYear / 2 == alexYear && brotherAlexYear % alexYear == 0) {
                    System.out.println(2020 + counterYears);
                    break;
                }
                alexYear++;
                if (alexYear >= brotherAlexYear) {
                    System.out.println("Никогда");
                    break;
                }
                counterYears++;
                brotherAlexYear++;
            }
        }
    }
}