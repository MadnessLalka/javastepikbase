import java.util.Scanner;

public class TernaryOperator4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int a = sc.nextInt(), b = sc.nextInt(), c = sc.nextInt();
        boolean isA = a < b + c, isB = b < a + c, isC = c < b + a;

        System.out.println((isA && isB && isC) ? "Yes":"No");
    }
}