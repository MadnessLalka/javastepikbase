import java.util.Scanner;

public class TernaryOperator3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double x = sc.nextDouble();
        double y = sc.nextDouble();

        System.out.println(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));
    }
}