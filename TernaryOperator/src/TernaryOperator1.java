import java.util.Scanner;

public class TernaryOperator1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean isVasya21Age = sc.nextInt() >= 21;

        if (isVasya21Age) {
            System.out.print("Да");
        } else {
            System.out.println("Нет");
        }
    }
}