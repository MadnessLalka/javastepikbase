import java.util.Scanner;

public class TernaryOperator2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println((sc.nextInt() >= 21) ? "Да" : "Нет");
    }
}