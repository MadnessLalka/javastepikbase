import java.util.Scanner;

public class TernaryOperator5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x1 = sc.nextInt(), y1 = sc.nextInt(), x2 = sc.nextInt(), y2 = sc.nextInt();

        boolean isX1Y1X2Y2MoreZero = (x1 > 0 && x2 > 0 && y1 > 0 && y2 > 0);
        boolean isXMoreZeroYLessZero = (x1 > 0 && x2 > 0 && y1 < 0 && y2 < 0);
        boolean isXLessZeroYMoreZero = (x1 < 0 && x2 < 0 && y1 > 0 && y2 > 0);
        boolean isXLessZeroYLessZero = (x1 < 0 && x2 < 0 && y1 < 0 && y2 < 0);

        if (isX1Y1X2Y2MoreZero || isXMoreZeroYLessZero || isXLessZeroYMoreZero || isXLessZeroYLessZero) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }

    }
}