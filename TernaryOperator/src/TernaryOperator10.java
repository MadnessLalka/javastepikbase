import java.util.Scanner;

public class TernaryOperator10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int countOfCows = sc.nextInt();
        boolean isCountOfCowsLess100 = countOfCows > 0 && countOfCows < 100;
        boolean isEndingOfTheWordA = countOfCows % 10 == 1 && countOfCows != 11;
        boolean isEndingOfTheWordЫ = countOfCows % 10 >= 2 && countOfCows % 10 <= 4 && countOfCows / 10 != 1;

        System.out.println(isCountOfCowsLess100 ? (isEndingOfTheWordA ? countOfCows + " корова" : (isEndingOfTheWordЫ ? countOfCows + " коровы" : countOfCows + " коров")) : "");

    }
}