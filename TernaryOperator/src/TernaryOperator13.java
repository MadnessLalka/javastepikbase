import java.util.Scanner;

public class TernaryOperator13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int x = sc.nextInt();

        boolean isXIncludeInTheRange = x >= 1 && x <= 100;

        if (isXIncludeInTheRange) {
            int hun = x / 10;
            int tenths = x % 10;

            if (x / 100 == 1) {
                System.out.println("C");
            }

            if (hun == 1) {
                System.out.print("X");
            } else if (hun == 2) {
                System.out.print("XX");
            } else if (hun == 3) {
                System.out.print("XXX");
            } else if (hun == 4) {
                System.out.print("XL");
            } else if (hun == 5) {
                System.out.print("L");
            } else if (hun == 6) {
                System.out.print("LX");
            } else if (hun == 7) {
                System.out.print("LXX");
            } else if (hun == 8) {
                System.out.print("LXXX");
            } else if (hun == 9) {
                System.out.print("XC");
            }

            if (tenths == 1) {
                System.out.println("I");
            } else if (tenths == 2) {
                System.out.println("II");
            } else if (tenths == 3) {
                System.out.println("III");
            } else if (tenths == 4) {
                System.out.println("IV");
            } else if (tenths == 5) {
                System.out.println("V");
            } else if (tenths == 6) {
                System.out.println("VI");
            } else if (tenths == 7) {
                System.out.println("VII");
            } else if (tenths == 8) {
                System.out.println("VIII");
            } else if (tenths == 9) {
                System.out.println("IX");
            }
        }
    }

}
