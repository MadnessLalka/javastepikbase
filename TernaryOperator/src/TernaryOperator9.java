import java.util.Scanner;

public class TernaryOperator9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x1 = sc.nextInt(), y1 = sc.nextInt(), x2 = sc.nextInt(), y2 = sc.nextInt();

        boolean isXOrYEqual = ((x1 == x2) || (y1 == y2)) || (Math.abs(x1 - x2) == Math.abs(y1 - y2));

        System.out.println((isXOrYEqual) ? "YES" : "NO");

    }
}