import java.util.Scanner;

public class TernaryOperator11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int coutOfKotletOnFridge = sc.nextInt();
        int fryingOnBothSidesMinutes = sc.nextInt();
        int countKotlet = sc.nextInt();

        if (countKotlet <= coutOfKotletOnFridge) {
            System.out.println(fryingOnBothSidesMinutes * 2);
        } else if ((countKotlet * 2) % coutOfKotletOnFridge == 0) {
            System.out.println((countKotlet * 2 / coutOfKotletOnFridge) * fryingOnBothSidesMinutes);
        } else if ((countKotlet * 2) % coutOfKotletOnFridge != 0) {
            System.out.println((countKotlet * 2 / coutOfKotletOnFridge) * fryingOnBothSidesMinutes + fryingOnBothSidesMinutes);
        }

    }
}