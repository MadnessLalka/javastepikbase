package src;

import java.util.Scanner;

public class BaseStringFunction1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char data = scanner.next().charAt(0);
        if (data == '0' ||
                data == '1' ||
                data == '2' ||
                data == '3' ||
                data == '4' ||
                data == '5' ||
                data == '6' ||
                data == '7' ||
                data == '8' ||
                data == '9') {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }

    }
}
