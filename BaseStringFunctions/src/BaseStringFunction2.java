import java.util.Scanner;

public class BaseStringFunction2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String data = scanner.nextLine();
        if (data.length() >= 5) {
            char[] d;

            System.out.println(data.charAt(2));
            System.out.println(data.charAt(data.length() - 2));
            d = new char[5];
            data.getChars(0, 5, d, 0);
            System.out.println(d);
            d = new char[data.length() - 2];
            data.getChars(0, (data.length() - 2), d, 0);
            System.out.println(d);
            d = new char[data.length() - 3];
            data.getChars(3, (data.length()), d, 0);
            System.out.println(d);
            System.out.println(data.length());
        }


    }
}