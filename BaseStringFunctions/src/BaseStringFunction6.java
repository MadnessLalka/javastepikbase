import java.util.Scanner;

public class BaseStringFunction6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String favoriteLetterMasha = scanner.nextLine().toLowerCase();
        String favoriteLetterOleg = scanner.nextLine().toLowerCase();
        int mashaWordsCounter = 0, olegWordsCounter = 0;
        String newspaperArticle = scanner.nextLine().toLowerCase();
        String[] newspaperArticleArray = newspaperArticle.split(" ");
        for (String word : newspaperArticleArray) {
            if (word.startsWith(favoriteLetterMasha) && word.endsWith(favoriteLetterOleg)) {
                mashaWordsCounter++;
            }
            if (word.startsWith(favoriteLetterOleg) && word.endsWith(favoriteLetterMasha)) {
                olegWordsCounter++;
            }
        }
        System.out.println(mashaWordsCounter + "\n" + olegWordsCounter);
    }
}