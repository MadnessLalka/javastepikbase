import java.util.Scanner;

public class BaseStringFunction8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfString = scanner.nextInt();
        scanner.nextLine();
        String[] recipeArray = new String[countOfString];
        String[] changeWordArray;

        for (int i = 0; i < countOfString; i++) {
            recipeArray[i] = scanner.nextLine();
        }

        countOfString = scanner.nextInt();
        scanner.nextLine();
        changeWordArray = new String[countOfString];

        for (int i = 0; i < countOfString; i++) {
            changeWordArray[i] = scanner.nextLine();
        }

        for (int i = 0; i < recipeArray.length; i++) {
            String[] splitArrayRecipe = recipeArray[i].split(" ");
            for (int j = 0; j < changeWordArray.length; j++) {
                String[] splitChangeWordArray = changeWordArray[j].split(" - ");
                for (String word : splitArrayRecipe) {
                    if (word.equalsIgnoreCase(splitChangeWordArray[0])) {
                        if (!word.equals(splitChangeWordArray[0])) {
                            String upCaseSearchWord = splitChangeWordArray[0].substring(0, 1).toUpperCase() + splitChangeWordArray[0].substring(1);
                            String upCaseChangeWord = splitChangeWordArray[1].substring(0, 1).toUpperCase() + splitChangeWordArray[1].substring(1);
                            recipeArray[i] = recipeArray[i].replace(upCaseSearchWord, upCaseChangeWord);
                        } else {
                            recipeArray[i] = recipeArray[i].replace(splitChangeWordArray[0], splitChangeWordArray[1]);
                        }
                    }
                }
            }
            System.out.println(recipeArray[i]);

        }
    }
}