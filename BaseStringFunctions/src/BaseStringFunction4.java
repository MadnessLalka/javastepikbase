import java.util.Scanner;

public class BaseStringFunction4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        scanner.nextLine();

        String[] stringArray = new String[n];
        String delimiter;
        String fullString = "";

        for (int i = 0; i < n; i++) {
            stringArray[i] = scanner.nextLine();
        }

        delimiter = scanner.next();

        fullString = String.join(delimiter, stringArray);

        System.out.println(fullString);

    }
}