import java.util.Scanner;

public class BaseStringFunction3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String allergicProduct = scanner.nextLine();
        String productForChange = scanner.nextLine();

        String recipe = scanner.nextLine();

        recipe = recipe.replace(allergicProduct, productForChange);
        System.out.println(recipe);

    }
}