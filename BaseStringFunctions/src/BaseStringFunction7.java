import java.util.Scanner;

public class BaseStringFunction7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String ip = scanner.nextLine();
        boolean isTrueIp = true;

        if (ip.length() <= 15) {
            String[] ipSubArray = ip.split("\\.");

            if (ipSubArray.length == 4) {
                for (int i = 0; i < ipSubArray.length; i++) {
                    if (ipSubArray[i].isBlank() || Integer.valueOf(ipSubArray[i]) < 0 || Integer.valueOf(ipSubArray[i]) > 255) {
                        isTrueIp = false;
                        break;
                    }
                }

            } else {
                isTrueIp = false;
            }

            if (isTrueIp) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }
}