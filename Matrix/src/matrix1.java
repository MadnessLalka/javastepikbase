import java.util.Scanner;

public class matrix1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numberCount = scanner.nextInt();
        double[] firstArray = new double[numberCount];
        double[] secondArray = new double[numberCount];

        for (int i = 0; i < firstArray.length; i++) firstArray[i] = scanner.nextDouble();
        for (int i = 0; i < secondArray.length; i++) secondArray[i] = scanner.nextDouble();

        for (int i = 0; i < numberCount; i++) {
            System.out.println(firstArray[i] + secondArray[i]);
        }
    }
}