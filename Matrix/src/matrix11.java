import java.util.Scanner;

public class matrix11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        if (n < 11) {
            int[][] matrix = new int[n][n];
            int sum = 0;
            int lineSum = 0;
            int columnSum = 0;
            int diagonalSum = 0;
            int reversDiagonalSum = 0;
            boolean isMagikMatrix = true;

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    matrix[i][j] = scanner.nextInt();
                    if (i == 0)
                        sum += matrix[i][j];
                }
            }

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    columnSum += matrix[j][i];
                    lineSum += matrix[i][j];
                    if (i == j) {
                        diagonalSum += matrix[i][j];
                    }
                    if (i + j == n - 1) {
                        reversDiagonalSum += matrix[i][j];
                    }
                }
            }
            if (lineSum / n != sum || columnSum / n != sum || diagonalSum != sum || reversDiagonalSum != sum) {
                isMagikMatrix = false;
            }

            if (isMagikMatrix) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }


    }
}