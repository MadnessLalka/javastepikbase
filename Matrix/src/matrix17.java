import java.util.Scanner;

public class matrix17 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if (n < 10) {
            int[][] matrix = new int[n][n];

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (i + j == n - 1) {
                        matrix[i][j] = 1;
                    } else if (i + j < n - 1) {
                        matrix[i][j] = 0;
                    } else {
                        matrix[i][j] = 2;
                    }
                    System.out.print(matrix[i][j] + " ");
                }
                System.out.println();
            }


        }
    }
}