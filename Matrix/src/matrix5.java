import java.util.Scanner;

public class matrix5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if (n < 100) {
            int[][] matrix = new int[n][n];
            int sumOfNumberLessForwardDiagonal = 0;
            int sumOfNumberMoreForwardDiagonal = 0;

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    matrix[i][j] = scanner.nextInt();
                    if (i > j) {
                        sumOfNumberLessForwardDiagonal += matrix[i][j];
                    } else if (i < j) {
                        sumOfNumberMoreForwardDiagonal += matrix[i][j];
                    }
                }
            }

            if (sumOfNumberLessForwardDiagonal == sumOfNumberMoreForwardDiagonal) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }


    }
}