import java.util.Scanner;

public class matrix9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt(), m = scanner.nextInt();
        int[][] matrix = new int[n][m];
        int[][] turnedMatrixRevers = new int[m][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = scanner.nextInt();
            }
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                turnedMatrixRevers[i][j] = matrix[j][(m - 1) - i];
                System.out.print(turnedMatrixRevers[i][j] + " ");
            }
            System.out.println();
        }


    }
}