import java.util.Scanner;

public class matrix16 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] array = new int[n];
        if (n >= 1 && n <= 100) {


            for (int i = 0; i < n; i++) {
                array[i] = scanner.nextInt();
            }

            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < (n - 1) - i; j++) {
                    if (array[j] > array[j + 1]) {
                        int swap = array[j + 1];
                        array[j + 1] = array[j];
                        array[j] = swap;

                    }
                }
            }

            for (int i = 0; i < array.length; i++) {
                System.out.print(array[i] + " ");
            }

        }

    }
}