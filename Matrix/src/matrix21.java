import java.util.Scanner;

public class matrix21 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt(), m = scanner.nextInt();
        int[][] matrixSportsMans = new int[n][m];
        int maxResult = -1;
        int countMaxResult = 0;
        String IMaxResult = "";

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrixSportsMans[i][j] = scanner.nextInt();
                if (matrixSportsMans[i][j] > maxResult) {
                    maxResult = matrixSportsMans[i][j];
                }
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (matrixSportsMans[i][j] == maxResult) {
                    countMaxResult++;
                    IMaxResult += i + " ";
                    break;
                }
            }
        }

        System.out.println(countMaxResult);
        System.out.println(IMaxResult);

    }
}