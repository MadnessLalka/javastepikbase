import java.util.Scanner;

public class matrix7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[][] jaggedArray = new int[n][];
        int maxSum;
        int sumArray = 0;
        int maxIndex = 0;

        for (int i = 0; i < n; i++) {
            int m = scanner.nextInt();
            int[] array = new int[m];
            for (int j = 0; j < m; j++) {
                array[j] = scanner.nextInt();
            }
            jaggedArray[i] = array;
        }

        maxSum = jaggedArray[0][0];


        for (int i = 0; i < n; i++) {
            for (int j = 0; j < jaggedArray[i].length; j++) {
                sumArray += jaggedArray[i][j];
            }
            if (sumArray > maxSum) {
                maxSum = sumArray;
                maxIndex = i;
            }
            sumArray = 0;
            System.out.println();
        }
        System.out.println(maxIndex + 1 + "\n" + maxSum);
        for (int i = 0; i < jaggedArray[maxIndex].length; i++)
            System.out.print(jaggedArray[maxIndex][i] + " ");
    }
}