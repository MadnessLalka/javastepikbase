import java.util.Scanner;

public class matrix12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] array = new int[n];

        if (n >= 1 && n <= 100) {
            for (int i = 0; i < n; i++) {
                array[i] = scanner.nextInt();
                if (i % 2 == 0) System.out.print(array[i] + " ");
            }
        }


    }
}