import java.util.Scanner;

public class matrix14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if (n >= 2 && n <= 10000) {
            int[] array = new int[n];
            boolean isEqualSign = false;
            int lastNumber = -1;

            for (int i = 0; i < n; i++) array[i] = scanner.nextInt();

            lastNumber = array[0];

            for (int i = 1; i < array.length; i++) {
                if ((lastNumber > 0 && array[i] > 0) || (lastNumber < 0 && array[i] < 0)) {
                    isEqualSign = true;
                    break;
                } else {
                    lastNumber = array[i];
                }
            }

            if (isEqualSign) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }

        }
    }
}