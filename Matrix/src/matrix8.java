import java.util.Scanner;

public class matrix8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt(), m = scanner.nextInt();
        int[][] matrix = new int[n][m];
        int[][] turnedMatrix = new int[m][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = scanner.nextInt();
            }
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                turnedMatrix[i][j] = matrix[(n - 1) - j][i];
                System.out.print(turnedMatrix[i][j] + " ");
            }
            System.out.println();
        }

    }
}