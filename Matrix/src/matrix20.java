import java.util.Scanner;

public class matrix20 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt(), m = scanner.nextInt();
        int[][] matrixSportsMans = new int[n][m];
        int maxResult = 0;
        int IMaxResult = 0, JMaxResult = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrixSportsMans[i][j] = scanner.nextInt();
                if (matrixSportsMans[i][j] > maxResult) {
                    maxResult = matrixSportsMans[i][j];
                    IMaxResult = i;
                    JMaxResult = j;
                }
            }
        }
        System.out.println(maxResult);
        System.out.println(IMaxResult + " " + JMaxResult);

    }
}