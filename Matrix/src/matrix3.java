import java.util.Scanner;

public class matrix3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt(), m = scanner.nextInt();
        int[][] multTable = new int[n][m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                multTable[i][j] = scanner.nextInt();
                System.out.print(multTable[i][j] + " ");
            }
            System.out.println();
        }

    }
}