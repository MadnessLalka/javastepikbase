import java.util.Scanner;

public class matrix15 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        if (n >= 3 && n <= 100) {
            int[] array = new int[n];
            int counterLessNumbers = 0;

            for (int i = 0; i < n; i++) {
                array[i] = scanner.nextInt();
            }

            for (int i = 1; i < array.length - 1; i++) {
                if (array[i] > array[i-1] && array[i] > array[i+1]){
                    counterLessNumbers++;
                }
            }
            System.out.println(counterLessNumbers);
        }

    }
}