import java.util.Scanner;

public class matrix18 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if (n < 100) {
            int[][] matrix = new int[n][n];
            int sumMoreDiagonal = 0;
            int sumLessDiagonal = 0;

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    matrix[i][j] = scanner.nextInt();
                    if (i > j) {
                        sumLessDiagonal += matrix[i][j];
                    } else if (i < j) {
                        sumMoreDiagonal += matrix[i][j];
                    }
                }
            }

            if (sumLessDiagonal == sumMoreDiagonal) System.out.println("YES");
            else System.out.println("NO");

        }
    }
}