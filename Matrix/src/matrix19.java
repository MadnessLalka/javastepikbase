import java.util.Scanner;

public class matrix19 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if (n < 11) {
            int[][] matrix = new int[n][n];
            int lineSum = 0;
            int columnSum = 0;
            int diagonalSum = 0;
            int reversDiagonalSum = 0;
            int sum = 0;
            boolean isMagikBox = true;

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    matrix[i][j] = scanner.nextInt();
                    if (i == 0) sum += matrix[i][j];
                }
            }

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {

                    lineSum += matrix[i][j];
                    columnSum += matrix[j][i];
                    if (i == j) {
                        diagonalSum += matrix[i][j];
                    }
                    if (i + j == n - 1) {
                        reversDiagonalSum += matrix[i][j];
                    }
                }
                if (lineSum != sum) {
                    isMagikBox = false;
                }
                if (columnSum != sum) {
                    isMagikBox = false;
                }

                lineSum = 0;
                columnSum = 0;

            }
            if (diagonalSum != sum){
                isMagikBox = false;
            }
            if (reversDiagonalSum != sum){
                isMagikBox = false;
            }


            if (isMagikBox) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }

        }
    }
}