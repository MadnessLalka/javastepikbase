import java.util.Scanner;

public class matrix6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt(), m = scanner.nextInt();

        if (n > 0 && m > 0) {
            int[][] matrix = new int[n][m];

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    matrix[i][j] = scanner.nextInt();
                }
            }

            int minElement = matrix[0][0];
            int minElementI = 0, minElementJ = 0;

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (matrix[i][j] < minElement) {
                        minElement = matrix[i][j];
                        minElementI = i;
                        minElementJ = j;
                    }
                }
            }

            System.out.println(minElementI + " " + minElementJ);
        } else {
            System.out.println(0 + " " + 0);
        }

    }
}