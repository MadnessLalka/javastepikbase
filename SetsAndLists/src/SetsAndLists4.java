import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SetsAndLists4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int countString = scan.nextInt();
        scan.nextLine();

        Map<Integer, String> personalNameList = new HashMap<>();
        Map<Integer, String> personalProfessionalList = new HashMap<>();

        if (countString <= 100) {
            for (int i = 0; i < countString; i++) {
                String name = scan.nextLine(), professional = scan.nextLine();
                personalNameList.put(i, name);
                personalProfessionalList.put(i, professional);
            }

            int countOfMaxProfession = 0;
            String nameMaxProfession = "";

            for (Map.Entry<Integer, String> item : personalProfessionalList.entrySet()) {
                String localMaxNameItem = item.getValue();
                int counterMaxItem = 0;

                for (Map.Entry<Integer, String> item1 : personalProfessionalList.entrySet()) {
                    if (localMaxNameItem.equals(item1.getValue())) {
                        counterMaxItem++;
                    }
                }

                if (counterMaxItem > countOfMaxProfession) {
                    countOfMaxProfession = counterMaxItem;
                    nameMaxProfession = localMaxNameItem;
                }
            }

            System.out.println(countOfMaxProfession);
            System.out.println(nameMaxProfession);
            for (Map.Entry<Integer, String> item : personalProfessionalList.entrySet()) {
                if (item.getValue().equals(nameMaxProfession)) {
                    System.out.println(personalNameList.get(item.getKey()));
                }

            }
        }
    }
}