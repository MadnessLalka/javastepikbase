import java.util.*;

public class SetsAndLists8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfKids = scanner.nextInt();
        scanner.nextLine();

        HashMap<String, String> uniqueWordMap = new HashMap<>();

        for (int i = 0; i < countOfKids; i++) {
            ArrayList<String> dividedString = new ArrayList<>();
            HashSet<String> setWordsKid = new HashSet<>();
            dividedString.addAll(List.of(scanner.nextLine().split(":")));
            String kidName = dividedString.get(0);
            setWordsKid.addAll(List.of(dividedString.get(1).toLowerCase().trim().split(" ")));
            uniqueWordMap.put(kidName, String.valueOf(setWordsKid));
        }

        int sizeWord = scanner.nextInt();
//        scanner.nextLine();
        int maxSizeKidString = 0;
        String nameMaxWordKid = "";

        for (String key : uniqueWordMap.keySet()) {
            ArrayList<String> tempWordList = new ArrayList<>();
            tempWordList.addAll(List.of(uniqueWordMap.get(key).split(", ")));
            int localCounterWordMoreSize = 0;
            for (String word : tempWordList) {
                if (word.length() >= sizeWord) localCounterWordMoreSize++;
            }

            if (localCounterWordMoreSize > maxSizeKidString) {
                maxSizeKidString = localCounterWordMoreSize;
                nameMaxWordKid = key;
            }
        }

        System.out.println(nameMaxWordKid + " " + maxSizeKidString);
    }
}