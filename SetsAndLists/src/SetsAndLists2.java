import java.util.*;

public class SetsAndLists2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        scanner.nextLine();

        HashMap<String, String> synonyms = new HashMap<>();

        for (int i = 0; i < n; i++) {
            String key = scanner.nextLine(), value = scanner.nextLine();
            synonyms.put(key, value);
            synonyms.put(value, key);
        }

        System.out.println(synonyms.get(scanner.nextLine()));

    }
}