import java.util.HashMap;
import java.util.Scanner;

public class SetsAndLists3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countString = scanner.nextInt();
        scanner.nextLine();

        HashMap<Integer, String> randomWords = new HashMap<>();

        for (int i = 0; i < countString; i++) {
            randomWords.put(i, scanner.nextLine());
        }

        int maxCountOfWord = 0;
        String maxWord = "";

        for (Integer ikey : randomWords.keySet()) {
            String iWord = randomWords.get(ikey);
            int maxCounter = 0;

            for (Integer jKey : randomWords.keySet()) {
                String jWord = randomWords.get(jKey);

                if (iWord.equals(jWord)) {
                    maxCounter++;
                }
            }

            if (maxCounter > maxCountOfWord) {
                maxCountOfWord = maxCounter;
                maxWord = iWord;
            }
        }

        System.out.println(maxWord);

    }
}