import java.util.ArrayList;
import java.util.Scanner;

public class SetsAndLists1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfEvenNumber = scanner.nextInt(), countOfInsertNumber = scanner.nextInt();
        scanner.nextLine();

        ArrayList<String> evenNumberList = new ArrayList<>();

        for (int i = 0; i < countOfEvenNumber; i++) {
            evenNumberList.add(String.valueOf(i + 1));
        }

        for (int i = 0; i < countOfInsertNumber; i++) {
            String valueInsertNumber = scanner.next(), indexInsertNumber = scanner.next();
            evenNumberList.add(Integer.parseInt(indexInsertNumber), valueInsertNumber);
        }

        for (String num : evenNumberList) {
            System.out.print(num + " ");
        }

    }
}