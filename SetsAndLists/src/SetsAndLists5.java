import java.util.HashSet;
import java.util.Scanner;

public class SetsAndLists5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int countOfNumbers = scan.nextInt();

        HashSet<Integer> numbers = new HashSet<>();

        for (int i = 0; i < countOfNumbers; i++) {
            numbers.add(scan.nextInt());
        }

        System.out.println(numbers.size());
    }
}