import java.util.HashSet;
import java.util.Scanner;

public class SetsAndLists6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        HashSet<Integer> numbers = new HashSet<>();

        int numberOfDigits = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < numberOfDigits; i++)
            System.out.println(!numbers.add(scanner.nextInt()) ? "Yes" : "No");
    }
}