import java.util.Scanner;

public class DataType13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int t = sc.nextInt();
        int x = sc.nextInt();
        int y = sc.nextInt();

        int travelTime = ((t * x) / y) + t;

        System.out.println(travelTime);
    }
}