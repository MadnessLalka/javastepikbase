import java.util.Scanner;

public class DataType9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int number = sc.nextInt();

        System.out.println((number / 100) + ((number / 10) % 10) + (number % 10));

    }
}