import java.util.Scanner;

public class DataType22 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        int countOfForeignWords = sc.nextInt();
        int countOfDays = sc.nextInt();

        System.out.println(countOfForeignWords * countOfDays);
    }
}
