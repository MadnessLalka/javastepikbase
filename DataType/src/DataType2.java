public class DataType2 {
    public static void main(String[] args) {
        int a = 5;
        float b = 0.4F;

        System.out.println(a + b);
        System.out.println(a - b);
        System.out.println(a * b);
        System.out.println(a / b);
    }
}