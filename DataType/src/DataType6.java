import java.util.Scanner;

public class DataType6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int mod = n - (n % 2) + 2;
        System.out.println(mod);
    }
}