import java.util.Scanner;

public class DataType7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int ruble = sc.nextInt();
        int kopeck = sc.nextInt();
        int countPye = sc.nextInt();

        int countKopecks = (kopeck * countPye) % 100;
        int countRubles = (ruble * countPye) + ((kopeck * countPye) / 100);

        System.out.println(countRubles + " " + countKopecks);
    }
}