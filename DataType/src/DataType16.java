import java.util.Scanner;

public class DataType16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int countOfPapers = sc.nextInt();
        int countOfPapersFromDay = sc.nextInt();

        int countOfPaperFromFiveDays = countOfPapers - (countOfPapersFromDay * 5);

        System.out.println(countOfPaperFromFiveDays);
    }
}