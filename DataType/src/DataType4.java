import java.util.Scanner;

public class DataType4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int integerNumber = sc.nextInt();

        System.out.println("The next number for the number " + integerNumber + " is " + (integerNumber + 1));
        System.out.println("The previous number for the number " + integerNumber + " is " + (integerNumber - 1));
    }
}