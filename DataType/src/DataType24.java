import java.util.Scanner;

public class DataType24 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        int valliSpeed = sc.nextInt();
        int evaSpeed = sc.nextInt();
        int pastTimeValli = sc.nextInt();
        int pastTimeEva = sc.nextInt();

        System.out.println((valliSpeed * pastTimeValli) + (evaSpeed * pastTimeEva));
    }
}
