import java.util.Scanner;

public class DataType17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int countOfPapers = sc.nextInt();
        int countOfPapersFromDay = sc.nextInt();
        int countDays = sc.nextInt();

        int countOfPaperFromDays = countOfPapers - (countOfPapersFromDay * countDays);

        System.out.println(countOfPaperFromDays);

    }
}