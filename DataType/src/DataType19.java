import java.util.Scanner;

public class DataType19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int oneFloorLiftSpeed = sc.nextInt();
        int numberFloorNeedRiseUp = sc.nextInt();

        int timeRiseUp = (oneFloorLiftSpeed * numberFloorNeedRiseUp) - oneFloorLiftSpeed;

        System.out.println(timeRiseUp);

    }
}