import java.util.Scanner;

public class DataType15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int hours = sc.nextInt();
        int minutes = sc.nextInt();

        int hourInADay = (hours % 24) * 60;
        int minuteInADay = minutes % 60;

        System.out.println(hourInADay + minuteInADay);


    }
}