import java.util.Scanner;

public class DataType18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int countOfChildren = sc.nextInt();
        int countOfApple = sc.nextInt();

        int childrenApple = countOfApple / countOfChildren;
        int ebeApple = countOfApple % countOfChildren;

        System.out.println(childrenApple + " " + ebeApple);
    }
}