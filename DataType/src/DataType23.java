import java.util.Scanner;

public class DataType23 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int countOfHorses = sc.nextInt();
        int countOfGooses = sc.nextInt();

        System.out.println((countOfHorses * 4) + (countOfGooses * 2));
    }
}
