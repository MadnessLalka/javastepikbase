import java.util.Scanner;

public class DataType14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int x = sc.nextInt();
        int t = sc.nextInt();
        int k = sc.nextInt();
        int n = sc.nextInt();

        int priceTeaPortion = t / 100;

        System.out.println((x * priceTeaPortion) + (x * (n * k)));

    }
}