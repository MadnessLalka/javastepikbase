import java.util.Scanner;

class Array13 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] += scan.nextInt();
        }
        int in1 = scan.nextInt(), in2 = scan.nextInt();
        System.out.println(array[in1] + array[in2]);

    }
}
