import java.util.Scanner;

class Array8 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) array[i] = scan.nextInt();

        int swap;
        for (int i = 0; i < n; i += 2) {
            if ((i + 1) < n) {
                swap = array[i + 1];
                array[i + 1] = array[i];
                array[i] = swap;
            }
        }

        for (int i = 0; i < n; i++) System.out.println(array[i]);
    }
}
