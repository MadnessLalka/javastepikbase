import java.util.Scanner;

class Array7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < n; i++) array[i] = scan.nextInt();

        for (int i = 0; i < array.length; i++) {
            int counterEqualNumber = 0;
            for (int j = 0; j < array.length; j++) {
                if (array[i] == array[j]) {
                    counterEqualNumber++;
                }
            }
            if (counterEqualNumber <= 1) {
                System.out.println(array[i]);
            }
        }


    }
}
