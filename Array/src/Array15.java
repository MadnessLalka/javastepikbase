import java.util.Scanner;

class Array15 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] += scan.nextInt();
        }

        int countOfQuery = scan.nextInt();
        for (int i = 0; i < countOfQuery; i++) {
            int indexSumOne = scan.nextInt(), indexSumTwo = scan.nextInt();
            if ((indexSumOne > array.length - 1 || indexSumOne < 0) || (indexSumTwo > array.length - 1 || indexSumTwo < 0)) {
                System.out.println("Error");
            } else {
                System.out.println(array[indexSumOne] + array[indexSumTwo]);
            }
        }


    }
}
