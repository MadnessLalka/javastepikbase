import java.util.Scanner;

class Array17 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        if (n > 1) {
            int[] array = new int[n];
            int counterMoreNextItems = 0;

            for (int i = 0; i < n; i++) {
                array[i] = scan.nextInt();
            }

            for (int i = 0; i < array.length - 1; i++) {
                if (array[i + 1] > array[i]) {
                    counterMoreNextItems++;
                }
            }
            System.out.println(counterMoreNextItems);
        }

    }
}
