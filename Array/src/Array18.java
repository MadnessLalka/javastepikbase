import java.util.Scanner;

class Array18 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        if (n > 0) {
            int[] array = new int[n];

            for (int i = 0; i < n; i++) {
                array[i] = scan.nextInt();
            }

            int elementOfArray = scan.nextInt();
            boolean isElementNotFound = true;

            for (int i = 0; i < array.length; i++) {
                if (elementOfArray == array[i]) {
                    System.out.println(i);
                    isElementNotFound = false;
                    break;
                }
            }

            if (isElementNotFound) {
                System.out.println("Error");
            }
        }

    }
}
