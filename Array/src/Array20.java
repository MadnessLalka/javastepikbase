import java.util.Scanner;

class Array20 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        if (n > 0) {
            int[] array = new int[n];

            for (int i = 0; i < n; i++) {
                array[i] = scan.nextInt();
            }

            int j = scan.nextInt(), r = scan.nextInt();

            int sumItems = 0;
            int counterAvg = 0;

            for (int i = j; i <= r; i++) {
                sumItems += array[i];
                counterAvg++;
            }

            System.out.println(sumItems / (float) counterAvg);
        }

    }

}

