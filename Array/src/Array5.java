import java.util.Scanner;

public class Array5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        int[] numberArray = new int[n];
        for (int i = 0; i < n; i++)
            numberArray[i] = scan.nextInt();

        int firstIndex = scan.nextInt(), secondIndex = scan.nextInt();

        for (int i = 0; i < n; i++)
            if (i == firstIndex)
                System.out.println(numberArray[secondIndex]);
            else if (i == secondIndex)
                System.out.println(numberArray[firstIndex]);
            else
                System.out.println(numberArray[i]);
    }
}