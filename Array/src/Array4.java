import java.util.Scanner;

public class Array4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        int[] numberArray = new int[n];
        int sumOfEvenNumbers = 0;

        for (int i = 0; i < n; i++) {
            numberArray[i] = scan.nextInt();
            if (i % 2 == 0)
                sumOfEvenNumbers+= numberArray[i];
        }

        System.out.println(sumOfEvenNumbers);

        for (int i = 0; i < numberArray.length; i+=2) {
            System.out.println(numberArray[i]);
        }

    }
}