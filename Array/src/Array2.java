import java.util.Scanner;

public class Array2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();

        int[] multiplierArray = new int[n];

        for (int i = 0; i < n; i++) {
            multiplierArray[i] = scan.nextInt();
        }

        int multiplier = scan.nextInt();

        for (int i = 0; i < multiplierArray.length; i++) {
            System.out.println(multiplierArray[i] * multiplier);
        }
    }
}