import java.util.Scanner;

class Array16 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        if (n > 1) {
            int[] array = new int[n];

            for (int i = 0; i < n; i++) {
                array[i] = scan.nextInt();
            }

            for (int i = 1; i < array.length; i++) {
                System.out.print(array[i - 1] + array[i] + " ");
            }
        }

    }
}
