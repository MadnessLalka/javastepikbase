import java.util.Scanner;

class Array9 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int a = scan.nextInt(), b = scan.nextInt();
        int[] cubeArray = new int[(b - a) + 1];

        for (int i = 0; i < cubeArray.length; i++)
            cubeArray[i] = (int) Math.pow((i + a), 3);

        int n = scan.nextInt();
        int[] numberArray = new int[n];

        for (int i = 0; i < n; i++) {
            numberArray[i] = scan.nextInt();
        }

        for (int i = 0; i < numberArray.length; i++) {
            if (numberArray[i] >= a && numberArray[i] <= b) {
                System.out.println(cubeArray[Math.abs(numberArray[i] - a)]);
            } else {
                System.out.println("Error");
            }
        }

    }
}
