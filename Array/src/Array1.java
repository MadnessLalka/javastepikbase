import java.util.Scanner;

public class Array1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = 1000;
        int[] cubeArray = new int[n];
        int a = scan.nextInt(), b = scan.nextInt();

        for (int i = 0; i < n; i++) {
            cubeArray[i] = (int) Math.pow((i + 1), 3);
        }
        System.out.println(cubeArray[a - 1] + "\n" + cubeArray[b - 1]);
    }
}