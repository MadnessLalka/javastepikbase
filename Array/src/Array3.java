import java.util.Scanner;

public class Array3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        int[] numberArray = new int[n];
        int sumOfAllArray = 0;

        for (int i = 0; i < n; i++) {
            numberArray[i] = scan.nextInt();
            sumOfAllArray+= numberArray[i];
        }

        System.out.println(sumOfAllArray);

        for (int i = n - 1; i >= 0; i--) {
            System.out.println(numberArray[i]);
        }

    }
}