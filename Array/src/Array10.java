import java.util.Scanner;

class Array10 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int countOfKegs = scan.nextInt(), countOfBalls = scan.nextInt();
        String[] kegsArray = new String[countOfKegs];

        for (int i = 0; i < countOfKegs; i++) kegsArray[i] = "I";

        for (int i = 0; i < countOfBalls; i++) {
            int startIndexKeg = scan.nextInt(), finishIndexKeg = scan.nextInt();
            for (int j = startIndexKeg - 1; j <= finishIndexKeg - 1; j++) {
                kegsArray[j] = ".";
            }
        }

        for (int i = 0; i < kegsArray.length; i++) System.out.print(kegsArray[i]);

    }
}
