import java.util.Scanner;

class Array19 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        if (n > 0) {
            int[] array = new int[n];

            for (int i = 0; i < n; i++) {
                array[i] = scan.nextInt();
            }

            int elementOfArray = scan.nextInt();
            int countOfEqualElements = 0;

            for (int i = 0; i < array.length; i++) {
                if (elementOfArray == array[i]) {
                    countOfEqualElements++;
                }
            }
            System.out.println(countOfEqualElements);
        }

    }
}
