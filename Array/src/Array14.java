import java.util.Scanner;

class Array14 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] += scan.nextInt();
        }

        int in1 = scan.nextInt(), in2 = scan.nextInt();
        if ((in1 > array.length - 1 || in1 < 0) || (in2 > array.length - 1 || in2 < 0)) {
            System.out.println("Error");
        } else {
            System.out.println(array[in1] + array[in2]);
        }


    }
}
