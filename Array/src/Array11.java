import java.util.Scanner;

class Array11 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] += i * i;
            System.out.print(array[i] + " ");
        }

    }
}
