import java.util.Scanner;

public class FunctionReturnRecursion4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String number = sc.nextLine();
        formatNumber(number);

    }

    static void formatNumber(String mobileNumber) {
        if (mobileNumber.length() != 11) {
            System.out.println("Неверное число символов");
            return;
        }
        char[] numberArray = mobileNumber.toCharArray();
        for (char num : numberArray) {
            if (!Character.isDigit(num)) {
                System.out.println("Не все символы являются цифрами");
                return;
            }
        }
        if (numberArray[0] != '8') {
            System.out.println("Это не российский номер телефона");
            return;
        }
        System.out.println(numberArray[0] + " (" + mobileNumber.substring(1, 4) + ") " + mobileNumber.substring(4, 7) + "-" + mobileNumber.substring(7, 9) + "-" + mobileNumber.substring(9, 11));
    }
}