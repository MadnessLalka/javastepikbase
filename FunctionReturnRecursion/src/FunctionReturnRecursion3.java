import java.util.Scanner;

public class FunctionReturnRecursion3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        sc.nextLine();
        int[][] mas = new int[n][];
        for (int i = 0; i < n; i++) {
            String line = sc.nextLine();
            String[] numbers = line.split(" ");
            int[] a = new int[numbers.length];
            for (int j = 0; j < numbers.length; j++)
                a[j] = Integer.parseInt(numbers[j]);
            mas[i] = a;
        }
        int ans = findZero(mas);
        if (ans != -1)
            System.out.println("0 in " + (ans + 1) + " line");
        else
            System.out.println("0 not found");

    }

    static int findZero(int[][] jaggedArrayNumber) {
        for (int i = 0; i < jaggedArrayNumber.length; i++) {
            for (int j = 0; j < jaggedArrayNumber[i].length; j++) {
                if (jaggedArrayNumber[i][j] == 0) {
                    return i;
                }
            }
        }
        return -1;

    }
}