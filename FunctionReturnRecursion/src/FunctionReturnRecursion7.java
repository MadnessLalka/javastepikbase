import java.util.Scanner;

public class FunctionReturnRecursion7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        sc.nextLine();
        int[][] mas = new int[n][];
        for (int i = 0; i < n; i++) {
            String line = sc.nextLine();
            String[] numbers = line.split(" ");
            int[] a = new int[numbers.length];
            for (int j = 0; j < numbers.length; j++)
                a[j] = Integer.parseInt(numbers[j]);
            mas[i] = a;
        }
        int ans = findLineMultipleSeven(mas);
        if (ans != -1)
            System.out.println("Сумма строки под номером " + (ans + 1) + " кратна 7");
        else
            System.out.println("Таких строк нет");

    }

    static int findLineMultipleSeven(int[][] jaggedIntegerMatrix) {
        for (int i = 0; i < jaggedIntegerMatrix.length; i++) {
            int lineSum = 0;
            for (int j = 0; j < jaggedIntegerMatrix[i].length; j++) {
                lineSum += jaggedIntegerMatrix[i][j];
            }

            if (lineSum % 7 == 0) return i;
        }
        return -1;
    }
}