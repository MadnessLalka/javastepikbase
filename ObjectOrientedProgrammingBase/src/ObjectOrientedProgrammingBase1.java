public class ObjectOrientedProgrammingBase1 {
    public static void main(String[] args) {
        Animal tiger = new Animal();
        tiger.type = "тигр";
        tiger.name = "Артём";
        tiger.age = 15;
        tiger.weight = 300.6;
        tiger.isSwim = true;
        tiger.isWalk = true;
        tiger.isFly = false;
        tiger.display();
        tiger.rename("Антон");
        tiger.display();

        Animal sparrow = new Animal();
        sparrow.type = "воробей";
        sparrow.name = "Капитан Джек";
        sparrow.age = 3;
        sparrow.weight = 2;
        sparrow.isSwim = false;
        sparrow.isWalk = true;
        sparrow.isFly = true;
        sparrow.display();
        sparrow.holiday(2);
        sparrow.display();

    }

    static class Animal {
        public String type;
        public String name;
        public int age;
        public double weight;
        boolean isFly;
        boolean isWalk;
        boolean isSwim;

        void display() {
            String answerByFly = (isFly) ? "Да" : "Нет";
            String answerByWalk = (isWalk) ? "Да" : "Нет";
            String answerBySwim = (isSwim) ? "Да" : "Нет";

            System.out.println("Тип: " + type + ", Имя:  " + name + ", Возраст: " + age + ", Вес: " + weight + ", Умеет летать: " + answerByFly + ", Умеет ходить: " + answerByWalk + ", Умеет плавать: " + answerBySwim);
        }

        void rename(String name) {
            this.name = name;
        }

        void holiday(int holidayCount) {
            for (int i = 0; i < holidayCount; i++) {
                this.weight += 0.1;
            }
        }


    }
}