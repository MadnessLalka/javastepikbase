package ObjectOrientedProgrammingBase2;

public class Animal {
    String type;
    String name;
    int age;
    double weight;
    boolean isFly;
    boolean isWalk;
    boolean isSwim;

    Animal(String type, String name){
        this.type = type;
        this.name = name;
    }

    Animal(String type, int age){
        this.type = type;
        this.age = age;
        this.name = "No Name";
    }

    Animal(String type, String name, int age, double weight, boolean isFly, boolean isWalk, boolean isSwim){
        this.type = type;
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.isFly = isFly;
        this.isWalk = isWalk;
        this.isSwim = isSwim;
    }

    void display() {
        String answerByFly = (isFly) ? "Да" : "Нет";
        String answerByWalk = (isWalk) ? "Да" : "Нет";
        String answerBySwim = (isSwim) ? "Да" : "Нет";

        System.out.println("Тип: " + type + ", Имя:  " + name + ", Возраст: " + age + ", Вес: " + weight + ", Умеет летать: " + answerByFly + ", Умеет ходить: " + answerByWalk + ", Умеет плавать: " + answerBySwim);
    }

    void rename(String name) {
        this.name = name;
    }

    void holiday(int holidayCount) {
        for (int i = 0; i < holidayCount; i++) {
            this.weight += 0.1;
        }
    }

}
