package ObjectOrientedProgrammingBase4;

public class WritingMaterials {
    private String name;
    private String color;
    private int price;
    private double length;
    private boolean isDraw;

    WritingMaterials(){

    }

    WritingMaterials(String name, String color) {
        this.name = name;
        this.color = color;
    }

    WritingMaterials(String name, int price) {
        this.name = name;
        this.price = price;
        this.color = "No Color";
    }

    WritingMaterials(int price, double length, boolean isDraw) {
        this.price = price;
        this.length = length;
        this.isDraw = isDraw;
        this.color = "No Color";
        this.name = "No Name";
    }

    public WritingMaterials(String name, String color, int price, double length, boolean isDraw) {
        this.name = name;
        this.color = color;
        this.price = price;
        this.length = length;
        this.isDraw = isDraw;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public boolean isDraw() {
        return isDraw;
    }

    public void setDraw(boolean draw) {
        isDraw = draw;
    }

    public void display() {
        String answerByDraw = (isDraw) ? "Да" : "Нет";
        System.out.println("Название: " + name + ", Цвет: " + color + ", Длина: " + length + ", Цена: " + price + ", Умеет рисовать: " + answerByDraw);
    }

    public void replaceRod(String color) {
        this.color = color;
    }

    public void priceUp(int priceIncrease) {
        this.price += priceIncrease;
    }

    public void priceDown(int priceReduction) {
        this.price -= priceReduction;
    }

    public void draw() {
        if (isDraw) {
            System.out.println(name + " провёл линию. Её цвет - " + color + ".");
        } else {
            System.out.println(name + " не может ничего нарисовать.");
        }
    }


}
