import java.util.Scanner;

public class StringAsArray1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char[] text = scanner.nextLine().toCharArray();
        int countOfCharF = 0;

        for (char ch: text){
            if (ch == 'f'){
                countOfCharF++;
            }
        }
        System.out.println(countOfCharF);
    }
}