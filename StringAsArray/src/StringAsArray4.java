import java.util.Scanner;

public class StringAsArray4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char[] text = scanner.nextLine().toCharArray();

        for (char symbol : text) {
            if (Character.isLetterOrDigit(symbol) || Character.isWhitespace(symbol)){
                System.out.print(symbol);
            }
        }

    }
}