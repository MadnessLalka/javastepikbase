import java.util.Scanner;

public class StringAsArray7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char[] textArray = scanner.nextLine().toLowerCase().toCharArray();
        int maximumCountOfSingleSymbol = 0, maximumCountOfAllSymbol = 0;
        String maximumSymbol = "";

        for (int i = 0; i < textArray.length; i++) {
            for (int j = i; j < textArray.length; j++) {
                if (textArray[i] == textArray[j] && textArray[i] != ' ') {
                    maximumCountOfSingleSymbol++;
                }
            }
            if (maximumCountOfAllSymbol < maximumCountOfSingleSymbol) {
                maximumCountOfAllSymbol = maximumCountOfSingleSymbol;
                maximumSymbol = String.valueOf(textArray[i]);
            } else if (maximumCountOfAllSymbol == maximumCountOfSingleSymbol && !maximumSymbol.equals(String.valueOf(textArray[i]))) {
                maximumSymbol += " " + textArray[i];
            }
            maximumCountOfSingleSymbol = 0;
        }

        String[] arrayMaxSymbol = maximumSymbol.split(" ");

        for (int i = 0; i < arrayMaxSymbol.length; i++) {
            for (int j = 0; j < arrayMaxSymbol.length - 1; j++) {
                if (arrayMaxSymbol[j].compareTo(arrayMaxSymbol[j + 1]) > 0) {
                    maximumSymbol = arrayMaxSymbol[j];
                    arrayMaxSymbol[j] = arrayMaxSymbol[j + 1];
                    arrayMaxSymbol[j + 1] = maximumSymbol;
                }
            }
        }

        System.out.println(arrayMaxSymbol[0] + " " + maximumCountOfAllSymbol);

    }
}