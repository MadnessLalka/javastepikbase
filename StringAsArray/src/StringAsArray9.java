import java.util.Scanner;

public class StringAsArray9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //2sdfsdgdg7562kdsvlkdnv343lsdkf23

        char[] string = scanner.nextLine().toCharArray();
        if (string.length <= 255) {
            String binaryNumber = "";
            String restoredString = "";
            int counterNeedLeftIteration = 0;

            for (int i = 0; i < string.length; i++) {
                if (counterNeedLeftIteration != 0) {
                    counterNeedLeftIteration--;
                } else {
                    if (Character.isDigit(string[i])) {
                        String digit = String.valueOf(string[i]);
                        if (i + 1 <= string.length - 1 && Character.isDigit(string[i + 1])) {
                            int counterArray = i + 1;
                            while (counterArray <= string.length - 1 && Character.isDigit(string[counterArray]) ) {
                                digit += string[counterArray];
                                counterArray++;
                                counterNeedLeftIteration++;
                            }
                        }
                        long intStringNumber = Long.parseLong(digit);
                        digit = "";
                        String convertedNumber = "";
                        if (intStringNumber == 0) {
                            restoredString += "0";
                            continue;
                        } else {
                            while (intStringNumber != 0) {
                                convertedNumber += intStringNumber % 2;
                                intStringNumber /= 2;
                            }
                        }

                        char[] convertedCharNumber = convertedNumber.toCharArray();
                        for (int j = convertedCharNumber.length - 1; j >= 0; j--) {
                            binaryNumber += convertedCharNumber[j];
                        }
                        restoredString += binaryNumber;
                        binaryNumber = "";
                    } else {
                        restoredString += string[i];
                    }
                }
            }
            System.out.println(restoredString);
        }
    }
}