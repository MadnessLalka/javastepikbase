import java.util.Scanner;

public class StringAsArray3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char[] text = scanner.nextLine().toLowerCase().toCharArray();
        int countDigit = 0, countLetter = 0;

        for (char symbol : text) {
            if (Character.isLetter(symbol)) {
                countLetter++;
            } else if (Character.isDigit(symbol)) {
                countDigit++;
            }
        }

        if (countDigit > countLetter) {
            System.out.println("Digit");
        } else if (countDigit == countLetter) {
            System.out.println("Equal");
        } else {
            System.out.println("Letter");
        }

    }
}