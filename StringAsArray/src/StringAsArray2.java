import java.util.Scanner;

public class StringAsArray2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char[] text = scanner.nextLine().toLowerCase().toCharArray();
        char symbol = scanner.nextLine().toLowerCase().charAt(0);
        int countSymbols = 0;

        for (char letter : text) {
            if (letter == symbol) {
                countSymbols++;
            }
        }
        System.out.println(countSymbols);

    }
}