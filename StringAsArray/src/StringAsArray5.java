import java.util.Scanner;

public class StringAsArray5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char[] text = scanner.nextLine().toLowerCase().toCharArray();
        String cloneTextString = "";
        String reversedText = "";

        for (char sym : text) {
            if (!Character.isWhitespace(sym)) {
                cloneTextString += sym;
            }
        }

        for (int i = cloneTextString.length() - 1; i >= 0; i--) {
            reversedText += cloneTextString.charAt(i);
        }

        if (cloneTextString.equals(reversedText)) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }


    }
}