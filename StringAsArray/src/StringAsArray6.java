import java.util.Scanner;

public class StringAsArray6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String text = scanner.nextLine();
        String[] words = text.split(" ");

        int maximumSymbols = 0;

        for (String sym : words) {
            if (sym.length() > maximumSymbols)
                maximumSymbols = sym.length();
        }
        System.out.println(maximumSymbols);
    }
}