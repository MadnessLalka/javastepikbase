import java.util.Scanner;

public class StringAsArray8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char[] number = scanner.nextLine().toCharArray();
        int counterZero = 0;

        for (char num : number) {
            if (num == '0' || num == '6' || num == '9') {
                counterZero++;
            } else if (num == '8') {
                counterZero += 2;
            }
        }
        System.out.println(counterZero);

    }
}