import java.util.Scanner;

public class Functions5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String figure = sc.nextLine();
        int x = sc.nextInt();

        perimeter(figure, x);
        area(figure, x);
    }

    static void perimeter(String figure, int value) {
        if (figure.equalsIgnoreCase("квадрат")) {
            System.out.println(value * 4);
        } else {
            System.out.println((int) Math.ceil(2 * Math.PI * value));
        }
    }

    static void area(String figure, int value) {
        if (figure.equalsIgnoreCase("квадрат")) {
            System.out.println((int) (Math.pow(value, 2)));
        } else {
            System.out.println((int) (Math.ceil(Math.PI * Math.pow(value, 2))));
        }
    }
}