import java.util.Scanner;

public class Functions8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] mas = new int[n];
        for (int i = 0; i < n; i++)
            mas[i] = sc.nextInt();
        maxMonotoneInterval(mas);
    }

    static void maxMonotoneInterval(int[] integerArray) {
        int counterNumberToInterval = 1;
        int maxCounter = 0;

        if (integerArray.length > 0) {
            maxCounter = 1;

            for (int i = 0; i < integerArray.length - 1; i++) {
                if (integerArray[i] <= integerArray[i + 1]) {
                    counterNumberToInterval++;
                } else {
                    counterNumberToInterval = 1;
                }

                if (counterNumberToInterval > maxCounter){
                    maxCounter = counterNumberToInterval;
                }

            }
        }

        System.out.println(maxCounter);
    }
}