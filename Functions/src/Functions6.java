import java.util.Scanner;

public class Functions6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();

        simple(a);
        simple(b);
        simple(c);
    }

    static void simple(int number) {
        int counterNumber = number;
        int counterDivNumber = 0;

        while (counterNumber > 0) {
            if (number % counterNumber == 0) {
                counterDivNumber++;
            }
            counterNumber--;
        }

        System.out.println((counterDivNumber == 2) ? "YES" : "NO");
    }
}