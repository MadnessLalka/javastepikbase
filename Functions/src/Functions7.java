import java.util.Scanner;

public class Functions7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int x = sc.nextInt();
        int y = sc.nextInt();

        simpleAB(x, y);
    }

    static void simpleAB(int start, int end) {
        if (start <= end) {
            for (int i = start; i <= end; i++) {
                if (i == 1) {
                    System.out.println("1 - " + i);
                } else {
                    int counterI = i;
                    int counterSimplexNumber = 0;

                    while (counterI > 0) {
                        if (i % counterI == 0) {
                            counterSimplexNumber++;
                        }
                        counterI--;
                    }

                    if (counterSimplexNumber == 2) {
                        System.out.println(i + " - простое");
                    } else {
                        System.out.println(i + " - составное");
                    }

                }
            }
        }
    }
}