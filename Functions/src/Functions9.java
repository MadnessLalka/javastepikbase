import java.util.*;

public class Functions9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        sortByLength(s);
    }

    static void sortByLength(String stringLine) {
        ArrayList<String> wordLine = new ArrayList<>();
        wordLine.addAll(List.of(stringLine.toLowerCase().split(" ")));

        Collections.sort(wordLine);

        for (int i = 0; i < wordLine.size(); i++) {
            for (int j = 0; j < wordLine.size() - 1; j++) {
                if (wordLine.get(j).length() > wordLine.get(j + 1).length()) {
                    String tempWordVariable = wordLine.get(j);
                    wordLine.set(j, wordLine.get(j + 1));
                    wordLine.set(j + 1, tempWordVariable);
                }
            }
        }

        for (String wd : wordLine) {
            System.out.print(wd + " ");
        }
    }
}
