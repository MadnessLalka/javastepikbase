import java.util.Scanner;

public class Functions3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s1 = scanner.nextLine();
        String s2 = scanner.nextLine();
        twoWords(s1, s2);

    }

    static void twoWords(String s1, String s2) {
        if (s1.length() > s2.length()) {
            System.out.println("Первая строка длиннее");
        } else if (s1.length() < s2.length()) {
            System.out.println("Вторая строка длиннее");
        } else {
            System.out.println("Равная длина");

        }
    }
}