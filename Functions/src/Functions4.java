import java.util.Scanner;

public class Functions4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double s1 = scanner.nextDouble();
        double s2 = scanner.nextDouble();
        double s3 = scanner.nextDouble();
        geron(s1, s2, s3);
    }

    static void geron(Double s1, Double s2, Double s3) {
        if (s1 > 0 && s2 > 0 && s3 > 0) {
            Double p = (s1 + s2 + s3) / 2;
            Double S = Math.sqrt(p * (p - s1) * (p - s2) * (p - s3));
            if (S > 0) {
                System.out.println(S);
            } else {
                System.out.println("Такого треугольника не существует");
            }
        }
    }
}