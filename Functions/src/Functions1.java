public class Functions1 {
    public static void main(String[] args) {
        myFirstFunction();
        myFirstFunction();
        myFirstFunction();
        mySecondFunction();
        mySecondFunction();
    }

    static void myFirstFunction(){
        System.out.println("Hello world!");
    }

    static void  mySecondFunction(){
        System.out.println("Java the best!");
    }
}