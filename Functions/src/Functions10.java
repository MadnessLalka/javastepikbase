import java.util.*;

public class Functions10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        sc.nextLine();
        int[][] mas = new int[n][];
        for (int i = 0; i < n; i++){
            String line = sc.nextLine();
            String[] numbers = line.split(" ");
            int[] a = new int[numbers.length];
            for (int j = 0; j < numbers.length; j++)
                a[j] = Integer.parseInt(numbers[j]);
            mas[i] = a;
        }
        maxiMin(mas);
    }

    static void maxiMin(int[][] integerJaggedArray){
        ArrayList<Integer> listMinimumNumber = new ArrayList<>();

        for (int i = 0; i < integerJaggedArray.length; i++) {
            int minNumber = integerJaggedArray[i][0];
            for (int j = 0; j < integerJaggedArray[i].length; j++) {
                if (minNumber > integerJaggedArray[i][j]){
                    minNumber = integerJaggedArray[i][j];
                }
            }
            listMinimumNumber.add(minNumber);
        }

        int maxNumber = listMinimumNumber.get(0);

        for (int number: listMinimumNumber){
            if(number > maxNumber){ maxNumber = number; }
        }

        System.out.println(maxNumber);
    }
}
