import java.util.Scanner;

public class Functions2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt(), b = scanner.nextInt(), c = scanner.nextInt();

        multiply(a, b, c);
    }

    static void multiply(int a, int b, int c) {
        System.out.println(a * b * c);
    }
}