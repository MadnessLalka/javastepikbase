import java.util.ArrayList;
import java.util.Scanner;

public class List9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfQuery = scanner.nextInt();
        scanner.nextLine();

        ArrayList<String> clothingList = new ArrayList<>();

        for (int i = 0; i < countOfQuery; i++) {
            String query = scanner.nextLine();

            if (query.contains("push")) {
                query = query.replace("push ", "");
                clothingList.add(query);
            } else if (query.contains("pop")) {
                System.out.println("Петя взял " + clothingList.get(clothingList.size()-1));
                clothingList.remove(clothingList.get(clothingList.size()-1));
            }
        }

        for (String cloth : clothingList) {
            System.out.print(cloth + " ");
        }

    }
}