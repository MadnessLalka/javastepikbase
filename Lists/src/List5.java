import java.util.ArrayList;
import java.util.Scanner;

public class List5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int sizeList = scanner.nextInt();
        ArrayList<String> symbolLists = new ArrayList<>();

        for (int i = 0; i < sizeList; i++) {
            symbolLists.add(Integer.toString(i));
        }

        int indexForReplaced = scanner.nextInt();

        if (indexForReplaced < sizeList) {
            String text = scanner.next();
            symbolLists.set(indexForReplaced, text);

            for (String sym : symbolLists) {
                System.out.print(sym + " ");
            }
        }
    }
}