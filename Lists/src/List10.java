import java.util.ArrayList;
import java.util.Scanner;

public class List10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfQuery = scanner.nextInt();
        scanner.nextLine();

        ArrayList<String> queryList = new ArrayList<>();
        ArrayList<String> processingQueryList = new ArrayList<>();

        int counterQuery = 0;

        for (int i = 0; i < countOfQuery; i++) {
            String query = scanner.nextLine();


            if (query.contains("Добавить")) {
                queryList.add(query.replace("Добавить ", ""));
                System.out.println(queryList.get(i) + " в очереди");

            } else if (query.contains("Следующий!")) {
                if (queryList.isEmpty()) {
                    System.out.println("Отлично, попью чаёк!");

                } else {
                    System.out.println(queryList.get(0) + " в обработке");
                    processingQueryList.add(queryList.get(0));
                    queryList.remove(queryList.get(0));
                    counterQuery = i;
                }

            } else if (query.contains(" занимал за ") || (query.contains(" занимала за "))) {
                String[] tempArray = query.split(" ");
                String nameBorrowedFor = tempArray[0];
                String nameGaveItToBorrow = tempArray[3];

                if (!queryList.contains(nameGaveItToBorrow)) {
                    if (processingQueryList.getLast().equals(nameGaveItToBorrow) && counterQuery + 1 == i) {
                        queryList.add(0, nameBorrowedFor);

                    } else {
                        System.out.println(nameBorrowedFor + ", не надо тут ля-ля");
                        queryList.add(nameBorrowedFor);

                    }
                } else {
                    if (queryList.contains(nameBorrowedFor)) {
                        queryList.remove(nameBorrowedFor);
                    }

                    queryList.add(queryList.indexOf(nameGaveItToBorrow) + 1, nameBorrowedFor);

                }

            } else if (query.contains("Посмотреть очередь")) {
                if (queryList.isEmpty()) {
                    System.out.println("*прокатилось перекати-поле*");

                } else {
                    String fullQuery = String.join(" ", queryList);
                    System.out.println(fullQuery);

                }

            } else if (query.contains("Стоит ли оно того?")) {
                if (queryList.size() < 5) {
                    queryList.add(query.replace("Стоит ли оно того? ", ""));
                } else {
                    System.out.println("Слабак!");
                }

            }
        }
    }
}

/*
4
Добавить Игорь
Добавить Антон
Антон занимал за Игорь
Посмотреть очередь

4
Добавить Антон
Добавить Игорь
Антон занимал за Игорь
Посмотреть очередь

5
Добавить Антон
Добавить Игорь
Посмотреть очередь
Антон занимал за Игорь
Посмотреть очередь

4
Добавить Игорь
Следующий!
Антон занимал за Игорь
Посмотреть очередь
*/