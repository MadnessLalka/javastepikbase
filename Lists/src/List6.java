import java.util.ArrayList;
import java.util.Scanner;

public class List6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int sizeList = scanner.nextInt();

        ArrayList<String> lists = new ArrayList<>();

        for (int i = 0; i < sizeList; i++) {
            lists.add(Integer.toString(i));
        }

        int indexRemove = scanner.nextInt();

        if (indexRemove < sizeList){
            lists.remove(indexRemove);

            for (String sym: lists){
                System.out.print(sym + " ");
            }
        }

    }
}