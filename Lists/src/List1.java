import java.util.Scanner;

public class List1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int integerNumber = scanner.nextInt();

        for (int i = 0; i < integerNumber; i++) {
            System.out.println(i + 1);
        }
    }
}