import java.util.Scanner;

public class List2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String text = scanner.nextLine().toLowerCase();

        if (text.contains(" повторение") || text.contains(" повторение ") || text.contains("повторение ") ){
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}