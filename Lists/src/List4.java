import java.util.ArrayList;
import java.util.Scanner;

public class List4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int sizeList = scanner.nextInt();
        ArrayList<String> symbolLists = new ArrayList<>();

        for (int i = 0; i < sizeList; i++) {
            symbolLists.add(Integer.toString(i));
        }

        int indexReplacedItem = scanner.nextInt();

        if (indexReplacedItem <= sizeList) {
            String dataReplacedItem = scanner.next();
            symbolLists.add(indexReplacedItem, dataReplacedItem);

            for (String sym : symbolLists) {
                System.out.print(sym + " ");
            }
        }

    }
}