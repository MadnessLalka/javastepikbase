import java.util.ArrayList;
import java.util.Scanner;

public class List8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfQuery = scanner.nextInt();
        scanner.nextLine();
        ArrayList<String> queryList = new ArrayList<>();

        for (int i = 0; i < countOfQuery; i++) {
            String newQuery = scanner.nextLine();

            if (newQuery.contains("Добавить ")) {
                queryList.add(newQuery.replace("Добавить ", ""));

            } else if (newQuery.contains("Следующий!")) {
                queryList.remove(0);

            } else if (newQuery.contains("занимала за ")) {
                int counterStandInLine = 0;
                int positionToArrayName = 0;
                ArrayList<String> finaleListName = new ArrayList<>();

                while (counterStandInLine < 2) {
                    char[] arrayName = newQuery.toCharArray();
                    String nameToLine = "";

                    for (int j = positionToArrayName; j < arrayName.length; j++) {
                        if (arrayName[j] != ' ') {
                            nameToLine += arrayName[j];
                            positionToArrayName++;
                        } else {
                            break;
                        }
                    }

                    positionToArrayName += " занимала за ".length();

                    finaleListName.add(nameToLine);
                    counterStandInLine++;

                    if (positionToArrayName >= arrayName.length) {
                        if (queryList.contains(finaleListName.get(0))) {
                            queryList.remove(finaleListName.get(0));
                        }
                        queryList.add(queryList.indexOf(finaleListName.get(1)) + 1, finaleListName.get(0));
                    }
                }
            }
        }

        String endQuery = "";
        for (String query : queryList) {
            endQuery += query + " ";
        }
        System.out.print(endQuery.trim());

    }
}