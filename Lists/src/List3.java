import java.util.ArrayList;
import java.util.Scanner;

public class List3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int evenNumber = scanner.nextInt();
        ArrayList<Integer> list = new ArrayList<>();

        list.add(1);
        for (int i = 0; i < evenNumber; i++) {
            list.add(0);
        }
        list.add(1);

        for (Integer num: list){
            System.out.print(num + " ");
        }

    }
}