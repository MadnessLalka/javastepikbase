import java.util.ArrayList;
import java.util.Scanner;

public class List7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfQuery = scanner.nextInt();
        scanner.nextLine();
        ArrayList<String> musicTrackLists = new ArrayList<>();
        int counterPlayingTrack = 0;

        for (int i = 0; i < countOfQuery; i++) {
            String command = scanner.nextLine();

            if (command.contains("push")) {
                musicTrackLists.add(command.replace("push ", "") + " ");
            } else if (command.contains("pop")) {
                musicTrackLists.set(counterPlayingTrack, ("воспроизводится " + musicTrackLists.get(counterPlayingTrack)));
                counterPlayingTrack++;
            }
        }

        for (String track : musicTrackLists) {
            if (track.contains("воспроизводится")){
                System.out.println(track.trim());
            } else {
                System.out.print(track);
            }
        }
    }
}